<?php

namespace App\Libraries;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\ConditionalFormatting\Wizard;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;

class Soal
{
    protected int $jumlah_pilgan = 0;
    protected int $bobot_pilgan = 0;
    protected int $jumlah_pilihan = 5;
    protected int $jumlah_essay = 0;
    protected Spreadsheet $sp;

    /**
     * @param array|object $options
     */
    public function __construct($options = null)
    {
        $this->sp = new Spreadsheet();
        $this->setOptions($options);
        $this->init();
    }

    protected function init()
    {
        $this->sp->getProperties()->setCreator('CBT Informatika')
            ->setLastModifiedBy('CBT Informatika')
            ->setTitle('Template Soal CBT')
            ->setSubject('Template Soal CBT')
            ->setDescription('Template Soal untuk aplikasi CBT Informatika')
            ->setKeywords('office 2007 openxml php cbt informatika')
            ->setCategory('Template');

        $this->sp->setActiveSheetIndex(0)
            ->setCellValue('A1', 'type')
            ->setCellValue('B1', 'soal')
            ->setCellValue('C1', 'keterangan')
            ->getColumnDimension('B')->setWidth(60);

        $row = 2;
        $col = "A";


        $greenStyle = new Style(false, true);
        $greenStyle->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()->setARGB(Color::COLOR_DARKGREEN);
        $greenStyle->getFill()
            ->getEndColor()->setARGB(Color::COLOR_DARKGREEN);
        $greenStyle->getFont()->setColor(new Color(Color::COLOR_WHITE));

        $redStyle = new Style(false, true);
        $redStyle->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()->setARGB(Color::COLOR_YELLOW);
        $redStyle->getFill()
            ->getEndColor()->setARGB(Color::COLOR_YELLOW);
        $redStyle->getFont()->setColor(new Color(Color::COLOR_BLACK));

        if ($this->jumlah_pilgan > 0) {
            for ($i = 0; $i < $this->jumlah_pilgan; $i++) {
                $this->sp->setActiveSheetIndex(0)
                    ->setCellValue("A{$row}", 'Pilgan')
                    ->setCellValue("B{$row}", 'Soal ' . $i + 1);
                $this->sp->getActiveSheet()->getStyle("A{$row}:C{$row}")->getFill()
                    ->setFillType(Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF9A17');
                $row++;
                $abc = "A";
                for ($x = 0; $x < $this->jumlah_pilihan; $x++) {
                    $this->sp->setActiveSheetIndex(0)->setCellValue("B{$row}", 'Pilihan' . $i + 1 . $abc++);
                    if ($x == 0) {
                        $this->sp->setActiveSheetIndex(0)->setCellValue("C{$row}", 'benar');
                    } else {
                        $this->sp->setActiveSheetIndex(0)->setCellValue("C{$row}", 'salah');
                    }
                    $row++;
                }
            }
            $cellRange = "C2:C{$this->getPilganRow()}";
            $conditionalStyles = [];
            $wizardFactory = new Wizard($cellRange);
            /** @var Wizard\TextValue $textWizard */
            $textWizard = $wizardFactory->newRule(Wizard::TEXT_VALUE);

            $textWizard->contains('benar')->setStyle($greenStyle);
            $conditionalStyles[] = $textWizard->getConditional();

            $textWizard->contains('salah')->setStyle($redStyle);
            $conditionalStyles[] = $textWizard->getConditional();

            $this->sp->getActiveSheet()->getStyle($textWizard->getCellRange())->setConditionalStyles($conditionalStyles);
        }

        if ($this->jumlah_essay > 0) {
            for ($i = 0; $i < $this->jumlah_essay; $i++) {
                $this->sp->setActiveSheetIndex(0)
                    ->setCellValue("A{$row}", 'Essay')
                    ->setCellValue("B{$row}", 'Soal ' . $i + 1)
                    ->setCellValue("C{$row}", 0);
                $this->sp->getActiveSheet()->getStyle("A{$row}:C{$row}")->getFill()
                    ->setFillType(Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('FFFF9A17');
                $row++;
            }
        }
    }

    /**
     * @param array|object $options
     */
    private function setOptions($options)
    {
        if (is_object($options)) {
            $options = (array)$options;
        }

        foreach ($options as $key => $value) {
            if (isset($this->{$key})) {
                $this->{$key} = $value;
            }
        }
    }

    public function download(string $filename = null)
    { // Rename worksheet
        $this->sp->getActiveSheet()->setTitle('Template');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->sp->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename ?? time() . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($this->sp, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function getPilganRow()
    {
        return ($this->jumlah_pilgan * $this->jumlah_pilihan) + $this->jumlah_pilgan + 1;
    }
}