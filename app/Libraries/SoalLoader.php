<?php

namespace App\Libraries;

use CodeIgniter\Files\File;
use CodeIgniter\Validation\Exceptions\ValidationException;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class SoalLoader
{
    protected array $pilgan = [];
    protected array $essay = [];
    protected Spreadsheet $sp;

    public function __construct(
        public File $file
    ) {
        helper('text');
        if (strtolower($file->getExtension()) != "xlsx") {
            throw new ValidationException('Ekstensi file harus berformat xlsx, format yang diupload ' . $file->getExtension());
        }
        $reader = new Xlsx();
        $this->sp = $reader->load($file);
        $this->init();
    }

    private function init(): void
    {
        $this->sp->setActiveSheetIndex(0);
        $worksheet = $this->sp->getActiveSheet();

        $pilgan = null;
        $essay = null;
        $idSoalPilgan = 0;
        foreach ($worksheet->getRowIterator() as $i => $row) {
            if ($i == 1) {
                continue;
            }
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $type = $cellIterator->seek('A')->current()->getValue();
            if ($type !== null) {
                if (strtolower($type) == 'pilgan') {
                    $poin = [
                        'type' => 1,
                        'soal' => $cellIterator->seek('B')->current()->getValue(),
                        'pilihan' => null,
                    ];
                    $pilgan[$idSoalPilgan++] = $poin;
                } elseif (strtolower($type) == 'essay') {
                    $nilai = $cellIterator->seek('C')->current()->getValue();
                    if ($nilai < 1) {
                        throw new ValidationException("Terdapat Essay yang masih memiliki nilai 0 pada baris $i, harap masukkan nilai yang sesuai");
                    }
                    $poin = [
                        'type' => 0,
                        'soal' => $cellIterator->seek('B')->current()->getValue(),
                        'nilai' => $nilai,
                    ];
                    $essay[] = $poin;
                }
            } else {
                $kebenaran = strtolower($cellIterator->seek('C')->current()->getValue()) == 'benar';
                $rowabc = [
                    'kode' => strtoupper(random_string('alnum', 8)),
                    'text' => $cellIterator->seek('B')->current()->getValue(),
                    'benar' => $kebenaran,
                ];
                $pilgan[$idSoalPilgan - 1]['pilihan'][] = $rowabc;
            }
        }

        if ($pilgan) {
            $this->pilgan = $pilgan;
        }
        if ($essay) {
            // Validasi nilai bobot
            $bobotEssay = array_sum(array_column($essay, 'nilai'));
            if ($pilgan) {
                $jumlahPilgan = sizeof($pilgan);
                $bobotPilgan = 100 - $bobotEssay;
                $modulus = $bobotPilgan % $jumlahPilgan;
                $saran = $modulus < ($jumlahPilgan / 2) ? $bobotPilgan - $modulus : $bobotPilgan - $modulus + $jumlahPilgan;
                if ($bobotPilgan < 1) throw new ValidationException('Total Nilai Harus 100, total nilai saat ini ' . $bobotPilgan + $bobotEssay);
                if ($modulus !== 0) throw new ValidationException("Bobot Pilgan harus dapat dibagi dengan $jumlahPilgan (jumlah pilgan), bobot saat ini $bobotPilgan, bobot pilgan yang disarankan adalah $saran");
            } else {
                if ($bobotEssay !== 100) throw new ValidationException('Total Nilai Harus 100, total nilai saat ini ' . $bobotEssay);
            }
            $this->essay = $essay;
        }
    }

    public function toArray(): array
    {
        $data = [
            'pilgan' => $this->pilgan,
            'essay' => $this->essay,
        ];
        return $data;
    }
}