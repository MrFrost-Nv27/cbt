<?php

declare(strict_types=1);

namespace Mrfrost\Cbt\Collectors;

use CodeIgniter\Debug\Toolbar\Collectors\BaseCollector;
use Mrfrost\Cbt\CbtCore;

/**
 * Debug Toolbar Collector for Cbt
 */
class CbtCollector extends BaseCollector
{
    protected $hasTimeline = false;

    protected $hasTabContent = true;

    protected $hasVarData = false;

    protected $title = 'CBT';

    private CbtCore $cbt;

    public function __construct()
    {
        $this->cbt = service('cbt');
    }

    public function getTitleDetails(): string
    {
        return 'Yeay';
    }

    /**
     * Returns the data of this collector to be formatted in the toolbar
     */
    public function display(): string
    {
        $html = '<p>Not logged in.</p>';

        return $html;
    }

    public function icon(): string
    {
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABG0lEQVR4nOWVwUpCQRSGP125MLBNC6Gdu8oC30Dd1kZwla6kJ/AtrLgbkfsAPkMQFPYOQQS6El2FLoQWQtwY+IXLoN6xaWP98MPMmXPOz51zzxlwxw0wBSZAi1/GKfAFvIpmffLTZGmgCXSBUHwGIuASuNL6KXbeVYyJTcS9EqzjQtx0fusiMAcGQEb70ZaEQ/lkFDNzEYj02fH7D2LXsWJg1SFU7M4CrthPgQPg0EfgQg22YlX2rJptDDz4CDxaf82H7DXtP4Gyj0AOKMWYlz0FFIGjvS/yGXDuI1C3mqst+zHQUw28ity3ivwmewVYAu9AwUdgF/wjgZk1rl1gfF9cx/XdlvkfJbDjImC6tGE9mWECje+1Yv8YvgF6HoveMP2QqAAAAABJRU5ErkJggg==';
    }
}