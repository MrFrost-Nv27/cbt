<?php

declare(strict_types=1);

namespace Mrfrost\Cbt\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\Shield\Authentication\Authenticators\Session;
use CodeIgniter\Shield\Traits\Viewable;
use Mrfrost\Cbt\CbtCore;

class CbtController extends BaseController
{
    use Viewable;

    protected $helpers = ['setting'];

    /**
     * Displays the form the login to the site.
     *
     * @return RedirectResponse|string
     */
    public function loginView(int $idUjian)
    {
        if (service('cbt')->loggedIn()) {
            return redirect()->to(config('CbtConfig')->loginRedirect());
        }
        return $this->view(setting('CbtConfig.views')['login'], ['idUjian' => $idUjian]);
    }

    /**
     * Attempts to log the user in.
     */
    public function loginAction(int $idUjian): RedirectResponse
    {
        if (!$this->validate([
            'token'  => 'required',
        ])) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        /** @var string|null $token */
        $token = $this->request->getPost('token');

        /** @var CbtCore $cbt */
        $cbt = service('cbt');

        // Attempt to login
        $result = $cbt->attempt($idUjian, $token);
        if (!$result->isOK()) {
            return redirect()->route('cbt-login', [$idUjian])->withInput()->with('error', $result->reason());
        }

        return redirect()->to(config('CbtConfig')->loginRedirect())->withCookies();
    }

    /**
     * Logs the current user out.
     */
    public function logoutAction(): RedirectResponse
    {
        // Capture logout redirect URL before auth logout,
        // otherwise you cannot check the user in `logoutRedirect()`.
        $url = config('Auth')->logoutRedirect();

        auth()->logout();

        return redirect()->to($url)->with('message', lang('Auth.successLogout'));
    }
}