<?php

namespace Mrfrost\Cbt\Models;

use CodeIgniter\Model;
use Mrfrost\Cbt\CbtOngoing;

class CbtModel extends Model
{
    protected $table            = 'cbt_ongoing';
    protected $returnType       = CbtOngoing::class;
    protected $allowedFields    = [
        'id_participant',
        'id_exam',
        'start',
        'stop',
    ];
    protected $validationRules = [];
}