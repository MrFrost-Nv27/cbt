<?php

declare(strict_types=1);

namespace Mrfrost\Cbt\Config;

use Mrfrost\Cbt\Collectors\CbtCollector;

class Registrar
{
    /**
     * Registers the Shield filters.
     */
    // public static function Filters(): array
    // {
    //     return [
    //         'aliases' => [
    //             'cbt' => SessionAuth::class,
    //         ],
    //     ];
    // }

    public static function Toolbar(): array
    {
        return [
            'collectors' => [
                CbtCollector::class,
            ],
        ];
    }
}