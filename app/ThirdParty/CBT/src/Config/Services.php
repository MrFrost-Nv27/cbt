<?php

declare(strict_types=1);

namespace Mrfrost\Cbt\Config;

use Config\Services as BaseService;
use Mrfrost\Cbt\CbtCore;

class Services extends BaseService
{
    /**
     * The base cbt class
     */
    public static function cbt(bool $getShared = true): CbtCore
    {
        if ($getShared) {
            return self::getSharedInstance('cbt');
        }

        $config = config('CbtConfig');

        return new CbtCore($config);
    }
}