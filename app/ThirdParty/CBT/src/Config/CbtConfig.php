<?php

declare(strict_types=1);

namespace Mrfrost\Cbt\Config;

use CodeIgniter\Config\BaseConfig;
use Mrfrost\Cbt\Models\CbtModel;

class CbtConfig extends BaseConfig
{
    public array $views = [
        'login'                       => 'Auth\Cbt\login',
        'layout'                      => 'Auth\layout',
    ];

    /**
     * --------------------------------------------------------------------
     * CBT Provider
     * --------------------------------------------------------------------
     * @var class-string<CbtModel>
     */
    public string $cbtProvider = CbtModel::class;

    public array $redirects = [
        'login'    => 'ujian-room',
        'logout'   => 'cbt-login',
    ];

    public function loginRedirect(): string
    {
        $url = setting('Auth.redirects')['login'];

        return $this->getUrl($url);
    }

    public function logoutRedirect(): string
    {
        $url = setting('Auth.redirects')['logout'];

        return $this->getUrl($url);
    }

    public function registerRedirect(): string
    {
        $url = setting('Auth.redirects')['register'];

        return $this->getUrl($url);
    }

    /**
     * @param string $url an absolute URL or a named route or just URI path
     */
    protected function getUrl(string $url): string
    {
        // To accommodate all url patterns
        $final_url = '';

        switch (true) {
            case strpos($url, 'http://') === 0 || strpos($url, 'https://') === 0: // URL begins with 'http' or 'https'. E.g. http://example.com
                $final_url = $url;
                break;

            case route_to($url) !== false: // URL is a named-route
                $final_url = rtrim(url_to($url), '/ ');
                break;

            default: // URL is a route (URI path)
                $final_url = rtrim(site_url($url), '/ ');
                break;
        }

        return $final_url;
    }
}