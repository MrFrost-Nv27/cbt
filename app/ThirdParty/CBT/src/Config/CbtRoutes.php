<?php

declare(strict_types=1);

namespace Mrfrost\Cbt\Config;

use CodeIgniter\Config\BaseConfig;

class CbtRoutes extends BaseConfig
{
    public array $routes = [
        'login' => [
            [
                'get',
                'login',
                'CbtController::loginView',
                'cbt-login', // Route name
            ],
            [
                'post',
                'login',
                'CbtController::loginAction',
            ],
        ],
        'logout' => [
            [
                'get',
                'logout',
                'CbtController::logoutAction',
                'cbt-logout',
            ],
        ],
    ];
}