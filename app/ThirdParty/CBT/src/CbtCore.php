<?php

declare(strict_types=1);

namespace Mrfrost\Cbt;

use App\Entities\EntityUjian;
use App\Models\ModelUjian;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\Router\RouteCollection;
use CodeIgniter\Shield\Entities\User;
use Mrfrost\Cbt\Config\CbtConfig;
use Mrfrost\Cbt\Controllers\CbtController;
use Mrfrost\Cbt\Entities\CbtEssay;
use Mrfrost\Cbt\Entities\CbtPilgan;
use Mrfrost\Cbt\Models\CbtModel;

class CbtCore
{
    // CBT Type states
    private const TYPE_PILGAN = 0;
    private const TYPE_ESSAY  = 1;

    /**
     * @var array<CbtPilgan>
     */
    public array $soalPilgan = [];

    /**
     * @var array<CbtEssay>
     */
    public array $soalEssay = [];

    /**
     * Authenticated or authenticating (pending login) User
     */
    protected ?EntityUjian $ujian = null;

    /**
     * The User auth state
     */
    private int $type  = self::TYPE_PILGAN;
    private int $index = 0;

    public function __construct(
        public CbtConfig $config
    ) {
    }

    public function routes(RouteCollection &$routes, array $config = []): void
    {
        $routes->group('cbt', ['namespace' => 'Mrfrost\Cbt\Controllers'], static function ($routes) {
            $routes->get('login/(:num)', [CbtController::class, 'loginView'], ['as' => 'cbt-login']);
            $routes->post('login/(:num)', 'CbtController::loginAction/$1');
        });
    }

    public function attempt(int $id, string $token): Result
    {
        /** @var IncomingRequest $request */
        $request = service('request');

        $this->ujian = model(ModelUjian::class)->find($id);
        $result = $this->check($id, $token);

        if (!$result->isOK()) {
            $this->ujian = null;
            return $result;
        }

        /** @var EntityUjian $user */
        $ujian = $result->extraInfo();

        $this->ujian = $ujian;
        return $result;
    }

    public function check(int $id, string $token): Result
    {
        $ujian = model(ModelUjian::class)->find($id);

        if ($ujian === null) {
            return new Result([
                'success' => false,
                'reason'  => lang('Auth.badAttempt'),
            ]);
        }

        // Now, try matching the passwords.
        if (!($ujian->token === $token)) {
            return new Result([
                'success' => false,
                'reason'  => 'Token tidak valid',
            ]);
        }

        return new Result([
            'success'   => true,
            'extraInfo' => $ujian,
        ]);
    }

    public function loggedIn(): bool
    {
        return false;
    }

    // public function login(User $user): void
    // {
    // }

    public function loginById($userId): void
    {
    }

    public function logout(): void
    {
    }

    public function getModel(): CbtModel
    {
        return model($this->config->cbtProvider);
    }
}