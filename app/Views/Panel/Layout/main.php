<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$this->renderSection('title')?></title>

    <link rel="stylesheet" href="<?=base_url('lib')?>/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/mdi.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/typicon.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/simplelineicon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/panel')?>/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?=base_url('assets/panel')?>/css/vertical-layout-light/style.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/tempusdominus.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/datatables.css">

    <script src="<?=base_url('lib')?>/umdreact.js"></script>
    <script src="<?=base_url('lib')?>/umdreacts.js"></script>
    <script src="<?=base_url('lib')?>/babel.js"></script>

</head>

<body class="bg-light">

    <main role="main" class="container-scroller">
        <?=$this->include('Panel\Layout\navbar')?>
        <div class="container-fluid page-body-wrapper">
            <?=$this->include('Panel\Layout\sidebar')?>
            <div class="main-panel">
                <div class="content-wrapper">
                    <?=$this->renderSection('main')?>
                </div>
                <?=$this->include('Panel\Layout\footer')?>
            </div>
        </div>
    </main>

    <script>
    const baseUrl = "<?=base_url()?>";
    const current = "<?=$active ?? 'dashboard'?>";
    </script>
    <script src="<?=base_url('lib')?>/feathericon.js"></script>
    <script src="<?=base_url('lib')?>/fontawesome.js"></script>
    <script src="<?=base_url('lib')?>/jquery.js"></script>
    <script src="<?=base_url('lib')?>/umdpopper.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/vendor.bundle.base.js"></script>
    <script src="<?=base_url('lib')?>/moment.js"></script>
    <script src="<?=base_url('lib')?>/oenasdas.js"></script>
    <script src="<?=base_url('lib')?>/dataformat.js"></script>
    <script src="<?=base_url('lib')?>/queeyprovider.js"></script>
    <script src="<?=base_url('lib')?>/progesbar.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/off-canvas.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/hoverable-collapse.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/template.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/settings.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/todolist.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/jquery.cookie.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/dashboard.js"></script>
    <script src="<?=base_url('lib')?>/cdn.js"></script>
    <script src="<?=base_url('lib')?>/basil.js"></script>
    <script>
    $('table').dataTable();
    $('.datetimepicker-init').tempusDominus();
    </script>
    <?=$this->renderSection('jsxComponent')?>
    <script src="<?=base_url('script')?>/app.js" type="module"></script>
    <?=$this->renderSection('pageScripts')?>
</body>

</html>