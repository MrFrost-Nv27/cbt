<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <?php foreach ($menu as $menuItem) : ?>
        <?= view_cell('SidebarMenu', ['menu' => $menuItem]); ?>
        <?php endforeach ?>
    </ul>
</nav>