<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$this->renderSection('title')?></title>

    <link rel="stylesheet" href="<?=base_url('lib')?>/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/mdi.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/typicon.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/simplelineicon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/panel')?>/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?=base_url('assets/panel')?>/css/vertical-layout-light/style.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/tempusdominus.css">
    <link rel="stylesheet" href="<?=base_url('lib')?>/datatables.css">

    <script src="<?=base_url('lib')?>/umdreact.js"></script>
    <script src="<?=base_url('lib')?>/umdreacts.js"></script>
    <script src="<?=base_url('lib')?>/babel.js"></script>
    <link rel="stylesheet" href="<?=base_url('lib')?>/sweetalert.css">
    <link rel="stylesheet" href="<?=base_url('style')?>/room.css">

</head>

<body>
    <div class="waktu">
        00:00:00
    </div>
    <div class="content-wrapper">
        <?php // $this->renderSection('main')
?>
        <div class="row grid-margin soal-wrapper">
            <div class="card-soal">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title d-flex justify-content-between align-items-center">
                            <button type="button" class="btn btn-outline-success btn-left"><span
                                    class="mdi mdi-arrow-left"></span></button>
                            <h4 class="card-title m-0"></h4>
                            <button type="button" class="btn btn-outline-success btn-right"><span
                                    class="mdi mdi-arrow-right"></span></button>
                        </div>
                        <div class="state-page">
                            <?=view_cell('FlashMessage::render')?>
                            <div class="soal-canvas">
                                <form class="form-soal" action="" method="post">
                                    <p class="soal-text"></p>
                                    <soal-answer class="soal-answer"></soal-answer>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="number-wrapper">
        <div class="number-toggler">
            <span class="mdi mdi-chevron-up"></span>
        </div>
        <div class="card number-card">
            <div class="card-body">
                <h5 class="card-title text-center">Nomor Soal</h5>
                <div class="number-view d-flex flex-wrap">
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php // $this->include('Panel\Layout\cbt_nav')
?>
    <script src="<?=base_url('lib')?>/basil.js"></script>
    <script>
    const ujianUrl = "<?=$ujianUrl?>";
    const baseUrl = "<?=base_url()?>";
    const current = "<?=$active ?? 'dashboard'?>";
    const basil = new window.Basil();
    </script>
    <script src="<?=base_url('lib')?>/feathericon.js"></script>
    <script src="<?=base_url('lib')?>/fontawesome.js"></script>
    <script src="<?=base_url('lib')?>/jquery.js"></script>
    <script src="<?=base_url('lib')?>/umdpopper.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/vendor.bundle.base.js"></script>
    <script src="<?=base_url('lib')?>/moment.js"></script>
    <script src="<?=base_url('lib')?>/oenasdas.js"></script>
    <script src="<?=base_url('lib')?>/dataformat.js"></script>
    <script src="<?=base_url('lib')?>/queeyprovider.js"></script>
    <script src="<?=base_url('lib')?>/progesbar.js"></script>
    <script src="<?=base_url('lib')?>/swal.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/off-canvas.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/hoverable-collapse.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/template.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/settings.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/todolist.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/jquery.cookie.js"></script>
    <script src="<?=base_url('assets/panel')?>/js/dashboard.js"></script>
    <script src="<?=base_url('lib')?>/cdn.js"></script>
    <script>
    const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener("mouseenter", Swal.stopTimer);
            toast.addEventListener("mouseleave", Swal.resumeTimer);
        },
    });
    $('table').dataTable();
    $('.datetimepicker-init').tempusDominus();
    </script>
    <?=$this->renderSection('jsxComponent')?>
    <script src="<?=base_url('script')?>/app.js" type="module"></script>
    <?=$this->renderSection('pageScripts')?>
</body>

</html>