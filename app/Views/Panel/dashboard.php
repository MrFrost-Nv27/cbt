<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">CBT Informatika</h4>
                <p class="card-description">
                    Computer Based Test
                </p>
                <p>
                    Selamat datang di sistem CBT Informatika Universitas Peradaban.
                </p>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>