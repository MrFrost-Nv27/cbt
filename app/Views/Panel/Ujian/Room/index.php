<?= $this->extend('Panel\Layout\cbt_main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="state-page">
    <?= view_cell('FlashMessage::render') ?>
    <div class="row grid-margin soal-wrapper">
        <div class="card-soal">
            <div class="card">
                <div class="card-body">
                    <div class="card-title d-flex justify-content-between align-items-center">
                        <button type="button" class="btn btn-outline-success">Left</button>
                        <h4 class="card-title m-0">Nomor</h4>
                        <button type="button" class="btn btn-outline-success">Right</button>
                    </div>
                    <div class="state-page">
                        <?= view_cell('FlashMessage::render') ?>
                        <div class="soal-canvas">
                            <div class="soal-text"></div>
                            <div class="soal-answer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>