<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title ?></title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@6.9.96/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.1.2/typicons.min.css" integrity="sha512-/O0SXmd3R7+Q2CXC7uBau6Fucw4cTteiQZvSwg/XofEu/92w6zv5RBOdySvPOQwRsZB+SFVd/t9T5B/eg0X09g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" integrity="sha512-QKC1UZ/ZHNgFzVKSAhV5v5j73eeL9EEN289eKAEFaAjgAiobVAnVv/AGuPbXsKl1dNoel3kNr6PYnSiTzVVBCw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/panel') ?>/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?= base_url('assets/panel') ?>/css/vertical-layout-light/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@eonasdan/tempus-dominus@6.2.10/dist/css/tempus-dominus.min.css" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.13.1/r-2.4.0/datatables.min.css" />

    <script src="https://unpkg.com/react@18/umd/react.production.min.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@18/umd/react-dom.production.min.js" crossorigin></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react-dom/18.2.0/umd/react-dom-server-legacy.browser.production.min.js" integrity="sha512-LLNANt3U1pNLpu5WTYM71K4wOa3zUTPqEfPpQmLZmr83LdSCSgkuZpDZu0m/pkSgo2AOwgAbjds46oVQb7lWhQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.3/dist/sweetalert2.min.css" integrity="sha256-sWZjHQiY9fvheUAOoxrszw9Wphl3zqfVaz1kZKEvot8=" crossorigin="anonymous">

</head>

<body class="bg-light">
    <main role="main" class="container-scroller">
        <div class="content-wrapper mt-4">
            <div class="row grid-margin justify-content-md-center text-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Token Ujian</h4>
                            <div class="state-page">
                                <?= view_cell('FlashMessage::render') ?>
                                <form action="<?= route_to('cbt-room-token') ?>" method="POST" id="token-form">
                                    <div class="container">
                                        <div class="row justify-content-center align-items-center">
                                            <div class="col-md-8">
                                                <div class="mb-3 row">
                                                    <div class="col">
                                                        <input type="text" class="form-control" id="token" name="token" placeholder="Masukkan Token Akses" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row text-center">
                                            <div class="col">
                                                <button type="submit" class="btn btn-success" id="token-submit">Masuk</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/basil.js/0.4.2/basil.min.js" integrity="sha512-YyWXLSSNMvQ+MDAfGLBVG/n02uJyA1PdPSeVes/j2tAD4m1REqG7OEZAeM3HQEqxBJoW/WF3x+TGP5192eQEZQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        const baseUrl = "<?= base_url() ?>";
        const tokenAttemptUrl = "<?= $attemptUrl ?>";
        const current = "<?= $active ?? 'dashboard' ?>";
        const basil = new window.Basil();
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.29.0/feather.min.js" integrity="sha512-24XP4a9KVoIinPFUbcnjIjAjtS59PUoxQj3GNVpWc86bCqPuy3YxAcxJrxFCxXe4GHtAumCbO2Ze2bddtuxaRw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://kit.fontawesome.com/3c5643e4eb.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" crossorigin="anonymous">
    </script>
    <script src="<?= base_url('assets/panel') ?>/js/vendor.bundle.base.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@eonasdan/tempus-dominus@6.2.10/dist/js/tempus-dominus.min.js" crossorigin="anonymous"></script>
    <script src="https://getdatepicker.com//6/js/plugins/customDateFormat.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/Eonasdan/tempus-dominus@master/dist/js/jQuery-provider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/progressbar.js/0.6.1/progressbar.min.js" integrity="sha512-7IoDEsIJGxz/gNyJY/0LRtS45wDSvPFXGPuC7Fo4YueWMNOmWKMAllEqo2Im3pgOjeEwsOoieyliRgdkZnY0ow==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
    <script src="<?= base_url('assets/panel') ?>/js/off-canvas.js"></script>
    <script src="<?= base_url('assets/panel') ?>/js/hoverable-collapse.js"></script>
    <script src="<?= base_url('assets/panel') ?>/js/template.js"></script>
    <script src="<?= base_url('assets/panel') ?>/js/settings.js"></script>
    <script src="<?= base_url('assets/panel') ?>/js/todolist.js"></script>
    <script src="<?= base_url('assets/panel') ?>/js/jquery.cookie.js"></script>
    <script src="<?= base_url('assets/panel') ?>/js/dashboard.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.13.1/r-2.4.0/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/basil.js/0.4.2/basil.min.js" integrity="sha512-YyWXLSSNMvQ+MDAfGLBVG/n02uJyA1PdPSeVes/j2tAD4m1REqG7OEZAeM3HQEqxBJoW/WF3x+TGP5192eQEZQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('table').dataTable();
        $('.datetimepicker-init').tempusDominus();
    </script>
    <?= $this->renderSection('jsxComponent') ?>
    <script src="<?= base_url('script') ?>/app.js" type="module"></script>
    <?= $this->renderSection('pageScripts') ?>
</body>

</html>