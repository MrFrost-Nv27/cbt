<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Master Data Ujian</h4>
                <p class="card-description">
                    Manajemen Data Ujian Informatika Universitas Peradaban
                </p>

                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <form
                        action="<?= $item ? route_to('backend-ujian-edit', $item->id) : route_to('backend-ujian-add') ?>"
                        method="POST">
                        <div class="container">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-md-8">
                                    <div class="mb-3 row">
                                        <label for="id_kuliah" class="col-sm-4 col-form-label">Perkuliahan</label>
                                        <div class="col-sm-8">
                                            <?php if ($item == null) : ?>
                                            <?= view_cell('SelectKuliah::render', ['selected' => $item->id_kuliah ?? null]) ?>
                                            <?php else : ?>
                                            <select class="form-select" aria-label="id_kuliah" disabled>
                                                <option value="" selected>
                                                    <?= $item->kuliah->keterangan ?>
                                                </option>
                                            </select>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="id_ruang" class="col-sm-4 col-form-label">Ruang</label>
                                        <div class="col-sm-8">
                                            <?= view_cell('SelectRuang::render', ['selected' => $item->id_ruang ?? null]) ?>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="alamat" class="col-sm-4 col-form-label">Waktu</label>
                                        <div class="col-sm-8">
                                            <div class="input-group log-event" id="waktupicker"
                                                data-td-target-input="nearest" data-td-target-toggle="nearest">
                                                <input id="waktu" name="waktu" type="text" class="form-control"
                                                    data-td-target="#waktupicker"
                                                    value="<?= $item?->waktu->toLocalizedString('dd-MM-Y HH:mm') ?? '' ?>"
                                                    required />
                                                <span class="input-group-text" data-td-target="#waktupicker"
                                                    data-td-toggle="datetimepicker">
                                                    <i class="mdi mdi-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="durasi" class="col-sm-4 col-form-label">Durasi (Menit)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="durasi" name="durasi"
                                                value="<?= $item->durasi ?? '' ?>" required />
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="type" class="col-sm-4 col-form-label">Tipe Ujian</label>
                                        <div class="col-sm-8">
                                            <?php if ($item == null) : ?>
                                            <select class="form-select" aria-label="type" name="type" id="type"
                                                required>
                                                <option value="" selected>Pilih Tipe Ujian</option>
                                                <option value="UTS">Ujian Tengah Semester</option>
                                                <option value="UAS">Ujian Akhir Semester</option>
                                            </select>
                                            <?php else : ?>
                                            <select class="form-select" aria-label="type" disabled>
                                                <option value="" selected>
                                                    <?= $item->type_detail ?>
                                                </option>
                                            </select>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?= route_to('ujian-data') ?>" class="btn btn-success">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<script>
tempusDominus.extend(tempusDominus.plugins.customDateFormat);
$('#waktupicker').tempusDominus({
    restrictions: {
        minDate: new Date(),
    },
    display: {
        buttons: {
            today: true,
            clear: true,
            close: false
        },
    },
    useCurrent: true,
    defaultDate: undefined,
    localization: {
        hourCycle: "h23",
        /**
         * This is only used with the customDateFormat plugin
         */
        dateFormats: {
            LTS: 'h:mm:ss T',
            LT: 'h:mm T',
            L: 'MM/dd/yyyy',
            LL: 'MMMM d, yyyy',
            LLL: 'dd-MM-yyyy HH:mm',
            LLLL: 'dddd, MMMM d, yyyy h:mm T'
        },
        /**
         * This is only used with the customDateFormat plugin
         */
        ordinal: (n) => n,
        /**
         * This is only used with the customDateFormat plugin
         */
        format: 'LLL'
    }
});
</script>
<?= $this->endSection() ?>