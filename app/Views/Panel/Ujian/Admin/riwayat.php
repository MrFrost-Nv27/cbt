<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Riwayat Data Ujian</h4>
                <p class="card-description">
                    Melihat riwayat ujian yang telah selesai
                </p>

                <?php if ($items) : ?>
                <div class="row my-4 text-center">
                    <div class="col">
                        <a href="<?= route_to('ujian-add') ?>" class="btn btn-danger"><i class="mdi mdi-delete"></i>
                            Hapus Semua Riwayat</a>
                    </div>
                </div>
                <?php endif ?>
                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Waktu</th>
                                    <th>Mata Kuliah</th>
                                    <th>Durasi</th>
                                    <th>Peserta</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($items) : ?>
                                <?php foreach ($items as $item) : ?>
                                <tr>
                                    <td><?= $item->type ?></td>
                                    <td><?= $item->waktu ?></td>
                                    <td><?= $item->matkul->nama ?></td>
                                    <td><?= $item->durasi ?> Menit</td>
                                    <td>0</td>
                                    <td>
                                        <a href="<?= route_to('ujian-delete', $item->id) ?>" role="button"
                                            class="btn badge rounded-pill text-bg-danger"><i
                                                class="mdi mdi-delete"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>