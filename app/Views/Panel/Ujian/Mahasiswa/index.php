<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">List Ujian</h4>
                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Waktu</th>
                                    <th>Mata Kuliah</th>
                                    <th>Durasi</th>
                                    <th>Ruang</th>
                                    <th>Tipe</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($items) : ?>
                                <?php foreach ($items as $item) : ?>
                                <tr>
                                    <td><?= $item->waktu ?> - <?= $item->deadline ?></td>
                                    <td><?= $item->matkul->nama ?></td>
                                    <td><?= $item->durasi ?> Menit</td>
                                    <td><?= $item->ruang->nama ?></td>
                                    <td><?= $item->type ?></td>
                                    <td>
                                        <?php if ($item->nilai) : ?>
                                        <div class="btn badge rounded-pill text-bg-primary"><?= $item->nilai ?></div>
                                        <?php elseif ($item->is_uploaded && $item->is_running) : ?>
                                        <a href="<?= route_to('cbt-room', $item->id) ?>" role="button"
                                            class="btn badge rounded-pill text-bg-success"><i
                                                class="mdi mdi-pen"></i></a>
                                        <?php else : ?>
                                        <span class="btn badge rounded-pill text-bg-warning"><i
                                                class="mdi mdi-clock"></i></span>
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>