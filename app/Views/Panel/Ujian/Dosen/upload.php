<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Upload file soal ujian</h4>
                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <div class="card mb-5">
                        <div class="card-body">
                            <h4 class="card-title text-center">Download Template file upload soal</h4>
                            <form action="<?= route_to('backend-ujian-template') ?>" method="POST">
                                <div class="container">
                                    <div class="row mb-4 align-items-md-end">
                                        <div class="col-md-3">
                                            <label for="jumlah_pilgan" class="form-label">Jumlah Pilgan</label>
                                            <input class="form-control" type="number" id="jumlah_pilgan" name="jumlah_pilgan" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="jumlah_pilihan" class="form-label">Tipe Pilgan</label>
                                            <select id="jumlah_pilihan" name="jumlah_pilihan" class="form-select" required>
                                                <option value="" selected>Pilih Banyak Pilihan</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <label for="jumlah_essay" class="form-label">Jumlah Essay</label>
                                            <input class="form-control" type="number" id="jumlah_essay"
                                                name="jumlah_essay" required>
                                        </div> -->
                                        <div class="col-md-3">
                                            <button type="submit" class="btn btn-success">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Upload File Soal</h4>
                            <form action="<?= route_to('backend-ujian-upload', $id) ?>" method="POST" enctype="multipart/form-data">
                                <div class="container">
                                    <div class="row justify-content-center align-items-center">
                                        <div class="col-md-8">
                                            <div class="mb-3 row">
                                                <label for="file" class="col-sm-2 col-form-label">File</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" type="file" id="file" name="file" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col">
                                            <button type="submit" class="btn btn-success">Upload</button>
                                            <a href="<?= route_to('ujian-dosen') ?>" class="btn btn-success">Batal</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>