<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Review Soal Ujian</h4>
                <p class="card-description">
                    <?= $item->type_detail ?> - <?= $item->kuliah->keterangan ?>
                </p>

                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <form action="<?= $item ? route_to('', $item->id) : route_to('backend-dosen-add') ?>" method="POST">
                        <div class="container">
                            <div class="row text-center">
                                <div class="col">
                                    <a href="<?= route_to('backend-ujian-reset', $item->id) ?>" class="btn btn-danger">Reset
                                        Soal</a>
                                    <a href="<?= route_to('ujian-dosen') ?>" class="btn btn-success">Kembali</a>
                                </div>
                            </div>
                            <div class="row justify-content-center align-items-center">
                                <div class="col-md-6">
                                    <?php if ($item->soal_pilgan) : ?>
                                        <!-- <h4 class="text-center my-4">Soal Pilihan Ganda</h4> -->
                                        <?php foreach ($item->soal_pilgan as $pilgan) : ?>
                                            <div class="mb-3 row bg-light">
                                                <p><?= $pilgan->soal ?></p>
                                                <div class="container px-4">
                                                    <?php $abc = 'A' ?>
                                                    <?php foreach ($pilgan->pilihan as $pilihan) : ?>
                                                        <div class="row<?= $pilihan->benar ? ' bg-success' : '' ?>">
                                                            <p><?= $abc++ ?>. <?= $pilihan->text ?></p>
                                                        </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                    <?php if ($item->soal_essay) : ?>
                                        <h4 class="text-center my-4">Soal Essay</h4>
                                        <?php foreach ($item->soal_essay as $essay) : ?>
                                            <div class="mb-3 row bg-light">
                                                <p><?= $essay->soal ?></p>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>