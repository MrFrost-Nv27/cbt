<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">List Ujian</h4>
                <p class="card-description">
                    List Ujian Prodi Informatika Universitas Peradaban
                </p>
                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Waktu</th>
                                    <th>Mata Kuliah</th>
                                    <th>Durasi</th>
                                    <th>Tipe</th>
                                    <th>Upload</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($items) : ?>
                                <?php foreach ($items as $item) : ?>
                                <tr>
                                    <td><?= $item->waktu ?></td>
                                    <td><?= $item->matkul->nama ?></td>
                                    <td><?= $item->durasi ?> Menit</td>
                                    <td><?= $item->type ?></td>
                                    <td><?= $item->getIcon($item->is_uploaded) ?></td>
                                    <td>
                                        <?php if ($item->is_uploaded) : ?>
                                        <a href="<?= route_to('ujian-soal', $item->id) ?>" role="button"
                                            class="btn badge rounded-pill text-bg-success"><i
                                                class="mdi mdi-eye"></i></a>
                                        <?php else : ?>
                                        <a href="<?= route_to('ujian-upload', $item->id) ?>" role="button"
                                            class="btn badge rounded-pill text-bg-warning"><i
                                                class="mdi mdi-upload"></i></a>
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>