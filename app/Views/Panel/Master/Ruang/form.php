<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Master Data Ruang</h4>
                <p class="card-description">
                    Manajemen Ruang Informatika Universitas Peradaban
                </p>

                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <form action="<?= $item ? route_to('backend-ruang-edit', $item->id) : route_to('backend-ruang-add') ?>" method="POST">
                        <div class="container">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-md-8">
                                    <div class="mb-3 row">
                                        <label for="kode" class="col-sm-2 col-form-label">Kode</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="kode" name="kode" value="<?= $item->kode ?? '' ?>">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="nama" name="nama" value="<?= $item->nama ?? '' ?>">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="kapasitas" class="col-sm-2 col-form-label">Kapasitas</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" id="kapasitas" name="kapasitas" value="<?= $item->kapasitas ?? '' ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?= route_to('master-ruang') ?>" class="btn btn-success">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>