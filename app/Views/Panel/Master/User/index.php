<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Master Data User</h4>
                <p class="card-description">
                    Manajemen Data User CBT Informatika Universitas Peradaban
                </p>
                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Role</th>
                                    <th>Nama User</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($items) : ?>
                                <?php foreach ($items as $item) : ?>
                                <tr>
                                    <td><?= $item->user->groups[0] ?></td>
                                    <td><?= $item->nama ?></td>
                                    <td><?= $item->user->email ?></td>
                                    <td>
                                        <a href="<?= route_to('master-user-edit', $item->id_user) ?>" role="button"
                                            class="btn badge rounded-pill text-bg-warning"><i
                                                class="mdi mdi-key-chain"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>