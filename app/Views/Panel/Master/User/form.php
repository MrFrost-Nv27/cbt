<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Master Data User</h4>
                <p class="card-description">
                    Ubah kata sandi user
                </p>
                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <form action="<?= route_to('backend-user-edit', $item->id) ?>" method="POST">
                        <div class="container">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-md-8">
                                    <div class="mb-3 row">
                                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="password" name="password"
                                                required />
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="passconf" class="col-sm-2 col-form-label">Konfirmasi
                                            Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="passconf" name="passconf"
                                                required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?= route_to('master-user') ?>" class="btn btn-success">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>