<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Master Data Mahasiswa</h4>
                <p class="card-description">
                    Manajemen Mahasiswa Informatika Universitas Peradaban
                </p>

                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <form
                        action="<?= $item ? route_to('backend-mahasiswa-edit', $item->id) : route_to('backend-mahasiswa-add') ?>"
                        method="POST">
                        <div class="container">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-md-8">
                                    <div class="mb-3 row">
                                        <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" id="nim" name="nim"
                                                value="<?= $item->nim ?? '' ?>" required>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="nama" name="nama"
                                                value="<?= $item->nama ?? '' ?>" required>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="alamat" name="alamat"
                                                value="<?= $item->alamat ?? '' ?>" required>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="username" class="col-sm-2 col-form-label">Username</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="username" name="username"
                                                value="<?= $item->user->username ?? '' ?>" required />
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="<?= $item->user->email ?? '' ?>" required />
                                        </div>
                                    </div>
                                    <?php if ($item == null) : ?>
                                    <?= view_cell('FormPassword::render') ?>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?= route_to('master-mahasiswa') ?>" class="btn btn-success">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>