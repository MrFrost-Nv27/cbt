<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Master Data Mahasiswa</h4>
                <p class="card-description">
                    Manajemen Mahasiswa Informatika Universitas Peradaban
                </p>

                <div class="row my-4 text-center">
                    <div class="col">
                        <a href="<?= route_to('master-mahasiswa-add') ?>" class="btn btn-primary"><i
                                class="mdi mdi-plus"></i> Tambah</a>
                    </div>
                </div>
                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($items) : ?>
                                <?php foreach ($items as $item) : ?>
                                <tr>
                                    <td><?= $item->nim ?></td>
                                    <td><?= $item->nama ?></td>
                                    <td>
                                        <a href="<?= route_to('master-mahasiswa-edit', $item->id) ?>" role="button"
                                            class="btn badge rounded-pill text-bg-warning"><i
                                                class="mdi mdi-pencil"></i></a>
                                        <a href="<?= route_to('backend-mahasiswa-delete', $item->id) ?>" role="button"
                                            class="btn badge rounded-pill text-bg-danger"><i
                                                class="mdi mdi-delete"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>