<?= $this->extend('Panel\Layout\main') ?>
<?= $this->section('title') ?> <?= $title ?> <?= $this->endSection() ?>
<?= $this->section('main') ?>

<div class="row grid-margin">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Master Data Studi</h4>
                <p class="card-description">
                    Manajemen Data Studi Mahasiswa Informatika Universitas Peradaban
                </p>

                <div class="state-page">
                    <?= view_cell('FlashMessage::render') ?>
                    <form
                        action="<?= $item ? route_to('backend-studi-edit', $item->id) : route_to('backend-studi-add') ?>"
                        method="POST">
                        <div class="container">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-md-8">
                                    <div class="mb-3 row">
                                        <label for="nim" class="col-sm-4 col-form-label">Perkuliahan</label>
                                        <div class="col-sm-8">
                                            <?php if ($item) : ?>
                                            <select class="form-select" aria-label="id_matkul" disabled required>
                                                <option value="" selected><?= $item->matkul->nama ?> -
                                                    <?= $item->dosen->nama ?> (<?= $item->ruang->nama ?>)</option>
                                            </select>
                                            <?php else : ?>
                                            <?= view_cell('SelectKuliah::render', ['selected' => $item->id_kuliah ?? null]) ?>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="id_mahasiswa" class="col-sm-4 col-form-label">Mahasiswa</label>
                                        <div class="col-sm-8">
                                            <?= view_cell('SelectMahasiswa::render', ['kuliah' => $item->id_mahasiswa ?? null, 'selected' => $item->id_mahasiswa ?? null]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?= route_to('master-studi') ?>" class="btn btn-success">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>