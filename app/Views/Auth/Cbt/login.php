<?= $this->extend(config('Auth')->views['layout']) ?>

<?= $this->section('title') ?><?= lang('Auth.login') ?> <?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="wrap-login100 p-l-55 p-r-55 p-t-45 p-b-45">
    <div class="text-center p-b-30">
        <span class="login100-form-title">
            CBT Room
        </span>
        <span><small>CBT Informatika UP</small></span>
    </div>
    <?php if (session('error') !== null) : ?>
    <div class="alert alert-danger" role="alert"><?= session('error') ?></div>
    <?php elseif (session('errors') !== null) : ?>
    <div class="alert alert-danger" role="alert">
        <?php if (is_array(session('errors'))) : ?>
        <?php foreach (session('errors') as $error) : ?>
        <?= $error ?>
        <br>
        <?php endforeach ?>
        <?php else : ?>
        <?= session('errors') ?>
        <?php endif ?>
    </div>
    <?php endif ?>

    <?php if (session('message') !== null) : ?>
    <div class="alert alert-success" role="alert"><?= session('message') ?></div>
    <?php endif ?>

    <form action="<?= url_to('cbt-login', $idUjian) ?>" method="post">
        <?= csrf_field() ?>
        <div class="wrap-input100 validate-input m-b-23" data-validate="Username is reauired">
            <span class="label-input100">Token</span>
            <input class="input100" name="token" type="text" placeholder="Masukkan Token" value="<?= old('token') ?>"
                required>
            <span class="focus-input100" data-symbol="&#xf183;"><span class="mdi mdi-qrcode"></span></span>
        </div>

        <div class="container-login100-form-btn m-t-8">
            <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <button class="login100-form-btn" type="submit">
                    <?= lang('Auth.login') ?>
                </button>
            </div>
        </div>
    </form>
</div>

<?= $this->endSection() ?>