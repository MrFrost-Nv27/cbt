<?= $this->extend(config('Auth')->views['layout']) ?>

<?= $this->section('title') ?><?= lang('Auth.login') ?> <?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="wrap-login100 p-l-55 p-r-55 p-t-45 p-b-45">
    <div class="text-center p-b-30">
        <span class="login100-form-title">
            <?= lang('Auth.login') ?>
        </span>
        <span><small>CBT Informatika UP</small></span>
    </div>
    <?php if (session('error') !== null) : ?>
    <div class="alert alert-danger" role="alert"><?= session('error') ?></div>
    <?php elseif (session('errors') !== null) : ?>
    <div class="alert alert-danger" role="alert">
        <?php if (is_array(session('errors'))) : ?>
        <?php foreach (session('errors') as $error) : ?>
        <?= $error ?>
        <br>
        <?php endforeach ?>
        <?php else : ?>
        <?= session('errors') ?>
        <?php endif ?>
    </div>
    <?php endif ?>

    <?php if (session('message') !== null) : ?>
    <div class="alert alert-success" role="alert"><?= session('message') ?></div>
    <?php endif ?>

    <form action="<?= url_to('login') ?>" method="post">
        <?= csrf_field() ?>
        <div class="wrap-input100 validate-input m-b-23" data-validate="Username is reauired">
            <span class="label-input100"><?= lang('Auth.email') ?></span>
            <input class="input100" name="email" type="email" placeholder="Masukkan <?= lang('Auth.email') ?>"
                value="<?= old('email') ?>" required>
            <span class="focus-input100" data-symbol="&#xf206;"></span>
        </div>

        <div class="wrap-input100 validate-input m-b-8" data-validate="Password is required">
            <span class="label-input100"><?= lang('Auth.password') ?></span>
            <input class="input100" type="password" name="password" placeholder="Masukkan <?= lang('Auth.password') ?>"
                required>
            <span class="focus-input100" data-symbol="&#xf190;"></span>
        </div>

        <?php if (setting('Auth.sessionConfig')['allowRemembering'] || setting('Auth.allowMagicLinkLogins')) : ?>
        <div class="row">
            <div class="col">
                <?php if (setting('Auth.sessionConfig')['allowRemembering']) : ?>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" name="remember" class="form-check-input" <?php if (old('remember')) : ?>
                            checked<?php endif ?>>
                        <?= lang('Auth.rememberMe') ?>
                    </label>
                </div>
                <?php endif; ?>
            </div>
            <div class="col">
                <?php if (setting('Auth.allowMagicLinkLogins')) : ?>
                <div class="text-right">
                    <?= lang('Auth.forgotPassword') ?> <a
                        href="<?= url_to('magic-link') ?>"><?= lang('Auth.useMagicLink') ?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>

        <div class="container-login100-form-btn m-t-8">
            <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <button class="login100-form-btn" type="submit">
                    <?= lang('Auth.login') ?>
                </button>
            </div>
        </div>

        <?php if (setting('Auth.allowRegistration')) : ?>
        <div class="flex-col-c p-t-27">
            <span class="txt1 p-b-17"><?= lang('Auth.needAccount') ?></span>
            <a class="txt2" href="<?= url_to('register') ?>"><?= lang('Auth.register') ?></a>
        </div>
        <?php endif ?>
    </form>
</div>

<?= $this->endSection() ?>