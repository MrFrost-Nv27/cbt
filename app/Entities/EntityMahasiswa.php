<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;
use CodeIgniter\Shield\Models\UserModel;

class EntityMahasiswa extends Entity
{
    protected $attributes = [
        'id'         => null,
        'id_user'    => null,
        'nim'       => null,
        'nama'       => null,
        'alamat'     => null,
        'created_at' => null,
        'updated_at' => null,
    ];
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'id'      => 'int',
        'id_user' => 'int',
        'nim'     => 'int',
        'nama'    => 'string',
        'alamat'  => 'string',
    ];

    public function getUser()
    {
        return model(UserModel::class)->findById($this->attributes['id_user']);
    }
}