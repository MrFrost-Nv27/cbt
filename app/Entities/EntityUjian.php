<?php

namespace App\Entities;

use App\Models\ModelDosen;
use App\Models\ModelKuliah;
use App\Models\ModelMahasiswa;
use App\Models\ModelMatkul;
use App\Models\ModelRuang;
use App\Models\ModelSoal;
use App\Models\ModelStudi;
use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;
use CodeIgniter\Validation\Exceptions\ValidationException;

class EntityUjian extends Entity
{
    protected $attributes = [
        'id'           => null,
        'id_kuliah'    => null,
        'id_ruang'     => null,
        'waktu'        => null,
        'durasi'       => null,
        'type'         => null,
        'token'        => null,
        'created_at'   => null,
        'updated_at'   => null,
    ];
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at', 'waktu'];
    protected $casts   = [
        'id'        => 'int',
        'id_kuliah' => 'int',
        'id_ruang'  => 'int',
        'durasi'    => 'int',
        'type'      => 'string',
        'done'      => 'boolean',
    ];

    public function getKuliah()
    {
        return model(ModelKuliah::class)->find($this->attributes['id_kuliah']);
    }

    public function getMatkul()
    {
        return model(ModelMatkul::class)->find($this->getKuliah()->id_matkul);
    }

    public function getDosen()
    {
        return model(ModelDosen::class)->find($this->getKuliah()->id_dosen);
    }

    public function getRuang()
    {
        return model(ModelRuang::class)->find($this->attributes['id_ruang']);
    }

    public function getIcon(bool $other = null)
    {
        $param = $other ?? $this->done;
        return $param ? '<span class="badge rounded-pill text-bg-success"><i class="mdi mdi-check-circle"></i></span>' : '<span class="badge rounded-pill text-bg-warning"><i class="mdi mdi-timer"></i></span>';
    }

    public function getDeadline(bool $isPassed = false)
    {
        return $isPassed ? $this->waktu->addMinutes($this->durasi)->isAfter(Time::now()) : $this->waktu->addMinutes($this->durasi);
    }

    public function getIsRunning()
    {
        return $this->waktu->isBefore(Time::now()) && $this->getDeadline(true);
    }

    public function getTypeDetail()
    {
        return $this->type == "UTS" ? "Ujian Tengah Semester" : "Ujian Akhir Semester";
    }

    public function getisUploaded()
    {
        $soal = model(ModelSoal::class)->where('id_ujian', $this->id)->first();
        return $soal !== null;
    }

    public function getSoal()
    {
        return model(ModelSoal::class)->where('id_ujian', $this->id)->findAll();
    }

    public function getSoalPilgan()
    {
        return model(ModelSoal::class)->where(['id_ujian' => $this->id, 'type' => 1])->findAll();
    }

    public function getSoalEssay()
    {
        return model(ModelSoal::class)->where(['id_ujian' => $this->id, 'type' => 0])->findAll();
    }

    public function getNilai()
    {
        $model = model(ModelMahasiswa::class);
        $studi = model(ModelStudi::class)->where(['id_kuliah' => $this->id_kuliah, 'id_mahasiswa' => $model->where('id_user', auth()->id())->first()->id])->first();
        return $studi->{strtolower($this->type)};
    }
}
