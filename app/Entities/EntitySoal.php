<?php

namespace App\Entities;

use App\Models\ModelPilgan;
use CodeIgniter\Entity\Entity;

class EntitySoal extends Entity
{
    protected $attributes = [
        'id'         => null,
        'id_ujian'   => null,
        'soal'       => null,
        'nilai'      => null,
        'type'       => null,
        'created_at' => null,
        'updated_at' => null,
    ];
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'id'         => 'int',
        'id_ujian'   => 'int',
        'soal'       => 'string',
        'nilai'      => 'int',
        'type'       => 'int',
    ];

    public function getPilihan()
    {
        return model(ModelPilgan::class)->where('id_soal', $this->id)->findAll();
    }

    public function getPilihanBenar()
    {
        return model(ModelPilgan::class)->where(['id_soal' => $this->id, 'benar' => true])->first();
    }

    public function getPilihanUjian()
    {
        return model(ModelPilgan::class)
            ->builder()
            ->select('id, id_ujian, id_soal, kode, text')
            ->where('id_soal', $this->id)
            ->get()->getResult();
    }
}