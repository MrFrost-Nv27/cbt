<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class EntityPilgan extends Entity
{
    protected $attributes = [
        'id'         => null,
        'id_ujian'   => null,
        'id_soal'    => null,
        'kode'       => null,
        'text'       => null,
        'created_at' => null,
        'updated_at' => null,
    ];
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'id'      => 'int',
        'id_ujian' => 'int',
        'id_soal' => 'int',
        'kode'    => 'string',
        'text'    => 'string',
        'benar'   => 'boolean',
    ];
}
