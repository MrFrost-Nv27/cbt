<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class EntityUpload extends Entity
{
    protected $attributes = [
        'jumlah_pilgan'  => null,
        'jumlah_pilihan' => null,
        'jumlah_essay'   => null,
    ];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at', 'waktu'];
    protected $casts   = [
        'jumlah_pilgan'  => 'int',
        'jumlah_pilihan' => 'int',
        'jumlah_essay'   => 'int',
    ];
}