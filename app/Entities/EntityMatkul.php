<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class EntityMatkul extends Entity
{
    protected $attributes = [
        'id'         => null,
        'kode'       => null,
        'nama'       => null,
        'sks'        => null,
        'created_at' => null,
        'updated_at' => null,
    ];
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'id'   => 'int',
        'kode' => 'string',
        'nama' => 'string',
        'sks'  => 'int',
    ];
}
