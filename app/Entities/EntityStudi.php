<?php

namespace App\Entities;

use App\Models\ModelDosen;
use App\Models\ModelKuliah;
use App\Models\ModelMahasiswa;
use App\Models\ModelMatkul;
use App\Models\ModelRuang;
use CodeIgniter\Entity\Entity;

class EntityStudi extends Entity
{
    protected $attributes = [
        'id'           => null,
        'id_kuliah'    => null,
        'id_mahasiswa' => null,
        'uts'          => null,
        'uas'          => null,
        'created_at'   => null,
        'updated_at'   => null,
    ];
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'id'           => 'int',
        'id_kuliah'    => 'int',
        'id_mahasiswa' => 'int',
        'uts'          => 'int',
        'uas'          => 'int',
    ];

    public function getKuliah()
    {
        return model(ModelKuliah::class)->find($this->attributes['id_kuliah']);
    }

    public function getMatkul()
    {
        return model(ModelMatkul::class)->find($this->getKuliah()->id_matkul);
    }

    public function getRuang()
    {
        return model(ModelRuang::class)->find($this->getKuliah()->id_ruang);
    }

    public function getDosen()
    {
        return model(ModelDosen::class)->find($this->getKuliah()->id_dosen);
    }

    public function getMahasiswa()
    {
        return model(ModelMahasiswa::class)->find($this->attributes['id_mahasiswa']);
    }
}