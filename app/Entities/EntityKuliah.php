<?php

namespace App\Entities;

use App\Models\ModelDosen;
use App\Models\ModelMatkul;
use App\Models\ModelRuang;
use CodeIgniter\Entity\Entity;

class Entitykuliah extends Entity
{
    protected $attributes = [
        'id'         => null,
        'id_matkul'  => null,
        'id_dosen'   => null,
        'id_ruang'   => null,
        'created_at' => null,
        'updated_at' => null,
    ];
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [
        'id'         => 'int',
        'id_matkul'  => 'int',
        'id_dosen'   => 'int',
        'id_ruang'   => 'int',
    ];

    public function getMatkul()
    {
        return model(ModelMatkul::class)->find($this->attributes['id_matkul']);
    }

    public function getDosen()
    {
        return model(ModelDosen::class)->find($this->attributes['id_dosen']);
    }

    public function getRuang()
    {
        return model(ModelRuang::class)->find($this->attributes['id_ruang']);
    }

    public function getKeterangan()
    {
        return "{$this->getMatkul()->nama} - {$this->getDosen()->nama} ({$this->getRuang()->nama})";
    }
}