<?php

namespace App\Cells;

use App\Models\ModelDosen;
use CodeIgniter\View\Cells\Cell;

class SelectDosen extends Cell
{
    public $items;
    public $selected;

    public function mount(?int $selected)
    {
        $this->items = model(ModelDosen::class)->findAll() ?? null;
        $this->selected = $selected;
    }
}