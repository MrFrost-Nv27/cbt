<?php

namespace App\Cells;

use App\Models\ModelMatkul;
use CodeIgniter\View\Cells\Cell;

class SelectMatkul extends Cell
{
    public $items;
    public $selected;

    public function mount(?int $selected, ?bool $kuliah)
    {
        $this->items = $kuliah ? model(ModelMatkul::class)->inputKuliah() ?? null : model(ModelMatkul::class)->findAll() ?? null;
        $this->selected = $selected;
    }
}