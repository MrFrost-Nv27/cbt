<?php if ($menu['header']) : ?>
<li class="nav-item nav-category"><?= $menu['title'] ?></li>
<?php endif ?>
<?php foreach ($menu['listmenu'] as $item) : ?>
<li class="nav-item">
    <?php if ($item['sub']) : ?>
    <a class="nav-link" data-bs-toggle="collapse" href="#<?= $item['id'] ?>" aria-expanded="false"
        aria-controls="<?= $item['id'] ?>">
        <i class="menu-icon mdi mdi-<?= $item['icon'] ?>"></i>
        <span class="menu-title"><?= $item['label'] ?></span>
        <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="<?= $item['id'] ?>">
        <ul class="nav flex-column sub-menu">
            <?php foreach ($item['listsub'] as $sub) : ?>
            <li class="nav-item"> <a class="nav-link" id="<?= $sub['id'] ?>"
                    href="<?= $sub['link'] ?>"><?= $sub['label'] ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
    <?php else : ?>
    <a class="nav-link" id="<?= $item['id'] ?>" href="<?= $item['link'] ?>">
        <i class="mdi mdi-<?= $item['icon'] ?> menu-icon"></i>
        <span class="menu-title"><?= $item['label'] ?></span>
    </a>
    <?php endif ?>
</li>
<?php endforeach ?>