<?php if ($items) : ?>
<select class="form-select" aria-label="id_mahasiswa" name="id_mahasiswa" id="id_mahasiswa" required>
    <option value="" selected>Pilih Mahasiswa</option>
    <?php foreach ($items as $item) : ?>
    <option value="<?= $item->id ?>"><?= $item->nama ?></option>
    <?php endforeach ?>
</select>
<?php else : ?>
<select class="form-select" aria-label="id_mahasiswa" disabled required>
    <option value="" selected>Tidak Ada Mahasiswa yang tersedia</option>
</select>
<?php endif ?>