<?php if ($items) : ?>
<select class="form-select" aria-label="id_kuliah" name="id_kuliah" id="id_kuliah" required>
    <option value="" selected>Pilih Perkuliahan</option>
    <?php foreach ($items as $item) : ?>
    <option value="<?= $item->id ?>"><?= $item->keterangan ?></option>
    <?php endforeach ?>
</select>
<?php else : ?>
<select class="form-select" aria-label="id_kuliah" disabled required>
    <option value="" selected>Tidak Ada Perkuliahan yang tersedia</option>
</select>
<?php endif ?>