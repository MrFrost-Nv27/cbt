<?php

namespace App\Cells;

use App\Models\ModelRuang;
use CodeIgniter\View\Cells\Cell;

class SelectRuang extends Cell
{
    public $items;
    public $selected;

    public function mount(?int $selected)
    {
        $this->items = model(ModelRuang::class)->findAll() ?? null;
        $this->selected = $selected;
    }
}