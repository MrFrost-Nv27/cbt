<?php

namespace App\Cells;

use App\Models\ModelMahasiswa;
use CodeIgniter\View\Cells\Cell;

class SelectMahasiswa extends Cell
{
    public $items;
    public $selected;

    public function mount(?int $selected, ?int $kuliah)
    {
        $this->items = $kuliah ? model(ModelMahasiswa::class)->inputKuliah($kuliah) ?? null : model(ModelMahasiswa::class)->findAll() ?? null;
        $this->selected = $selected;
    }
}