<?php if ($items) : ?>
<select class="form-select" aria-label="id_matkul" name="id_matkul" id="id_matkul" required>
    <option value="" selected>Pilih Mata Kuliah</option>
    <?php foreach ($items as $item) : ?>
    <option value="<?= $item->id ?>"><?= $item->nama ?></option>
    <?php endforeach ?>
</select>
<?php else : ?>
<select class="form-select" aria-label="id_matkul" disabled required>
    <option value="" selected>Tidak Ada Matkul yang tersedia</option>
</select>
<?php endif ?>