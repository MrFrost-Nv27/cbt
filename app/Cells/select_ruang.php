<?php if ($items) : ?>
<select class="form-select" aria-label="id_ruang" name="id_ruang" id="id_ruang" required>
    <option value="" <?= $selected == null ? "selected" : null ?>>Pilih Ruang</option>
    <?php foreach ($items as $item) : ?>
    <option value="<?= $item->id ?>" <?= $selected == $item->id ? "selected" : null ?>><?= $item->nama ?></option>
    <?php endforeach ?>
</select>
<?php else : ?>
<select class="form-select" aria-label="id_ruang" disabled required>
    <option value="" selected>Tidak Ada Matkul yang tersedia</option>
</select>
<?php endif ?>