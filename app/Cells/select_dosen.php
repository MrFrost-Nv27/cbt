<?php if ($items) : ?>
<select class="form-select" aria-label="id_dosen" name="id_dosen" id="id_dosen" required>
    <option value="" <?= $selected == null ? "selected" : null ?>>Pilih Dosen</option>
    <?php foreach ($items as $item) : ?>
    <option value="<?= $item->id ?>" <?= $selected == $item->id ? "selected" : null ?>><?= $item->nama ?></option>
    <?php endforeach ?>
</select>
<?php else : ?>
<select class="form-select" aria-label="id_dosen" disabled required>
    <option value="" selected>Tidak Ada Dosen yang tersedia</option>
</select>
<?php endif ?>