<?php

namespace App\Cells;

use CodeIgniter\View\Cells\Cell;

class SidebarMenu extends Cell
{
    public string $title = 'judul';
    public array $menu = [];
}