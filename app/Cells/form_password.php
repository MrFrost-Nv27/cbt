<div class="mb-3 row">
    <label for="password" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
        <input type="password" class="form-control" id="password" name="password" required />
    </div>
</div>
<div class="mb-3 row">
    <label for="password_confirm" class="col-sm-2 col-form-label">Konfirmasi
        Password</label>
    <div class="col-sm-10">
        <input type="password" class="form-control" id="password_confirm" name="password_confirm" required />
    </div>
</div>