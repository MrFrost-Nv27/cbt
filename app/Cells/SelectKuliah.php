<?php

namespace App\Cells;

use App\Models\ModelKuliah;
use CodeIgniter\View\Cells\Cell;

class SelectKuliah extends Cell
{
    public $items;
    public $selected;

    public function mount(?int $selected)
    {
        $this->items = model(ModelKuliah::class)->findAll() ?? null;
        $this->selected = $selected;
    }
}