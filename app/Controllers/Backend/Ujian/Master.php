<?php

namespace App\Controllers\Backend\Ujian;

use App\Controllers\BaseBackend;
use App\Entities\EntityPilgan;
use App\Entities\EntitySoal;
use App\Entities\EntityUjian;
use App\Entities\EntityUpload;
use App\Libraries\Soal;
use App\Libraries\SoalLoader;
use App\Models\ModelPilgan;
use App\Models\ModelSoal;
use App\Models\ModelUjian;
use CodeIgniter\I18n\Time;
use CodeIgniter\Validation\Exceptions\ValidationException;

class Master extends BaseBackend
{
    protected string $model = ModelUjian::class;
    protected string $entity = EntityUjian::class;
    protected string $routeSuccess = 'ujian-data';

    protected function beforeSave(array $data)
    {
        helper('text');
        if ($data['id'] === null) {
            if (model($this->getModel())->where([
                'id_kuliah' => $data['post']['id_kuliah'],
                'type' => $data['post']['type'],
                'done' => 0,
            ])->first()) {
                return redirect()->back()->withInput()->with('error', 'Masih terdapat Ujian yang belum selesai pada kuliah tersebut');
            }
        }
        $data['post']['token'] = strtoupper(random_string('alnum', 8));
        return $data;
    }

    public function template()
    {
        $post = $this->request->getPost();
        $data = new EntityUpload($post);
        try {
            $soal = new Soal($data->toArray());
            $soal->download();
        } catch (\Throwable $th) {
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }

    public function upload(int $id)
    {
        $file = $this->request->getFile('file');
        $modelSoal = model(ModelSoal::class);
        $modelPilgan = model(ModelPilgan::class);
        try {
            if ($file->isFile()) {
                $loader = new SoalLoader($file);
                $soal = $loader->toArray();
                if ($soal['pilgan']) {
                    foreach ($soal['pilgan'] as $pilgan) {
                        $item = new EntitySoal([
                            'id_ujian' => $id,
                            'soal'     => $pilgan['soal'],
                            'type'     => $pilgan['type'],
                        ]);
                        $insertID = $modelSoal->insert($item);
                        for ($i = 0; $i < count($pilgan['pilihan']); $i++) {
                            $pilgan['pilihan'][$i]['id_ujian'] = $id;
                            $pilgan['pilihan'][$i]['id_soal'] = $insertID;
                        }
                        $pilihan = $pilgan['pilihan'];
                        if ($modelPilgan->insertBatch($pilihan) == false) {
                            dd(array_is_list($pilihan));
                            throw new ValidationException('gagal menambahkan pilihan');
                        }
                    }
                }
                if ($soal['essay']) {
                    foreach ($soal['essay'] as $essay) {
                        $item = new EntitySoal($essay);
                        $item->id_ujian = $id;
                        if (!$modelSoal->save($item)) {
                            throw new ValidationException('gagal menambahkan essay');
                        }
                    }
                }
                return redirect()->route('ujian-dosen')->with('message', 'Soal berhasil diupload');
            }
            throw new ValidationException('Gagal Mengupload File');
        } catch (\Throwable $th) {
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }

    public function setDone(int $id)
    {
        $item = model($this->getModel())->find($id);
        try {
            if ($item->getDeadline(true)) {
                throw new ValidationException("Ujian belum dapat diselesaikan karena belum berakhir sesuai dengan seharusnya, yaitu pada {$item->deadline}");
            }
            $item->done = true;
            if (model($this->getModel())->save($item)) {
                return redirect()->route($this->routeSuccess)->with('message', 'Ujian berhasil diselesaikan');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }

    public function reset(int $id)
    {
        $model = model(ModelSoal::class);
        $soal = $model->where('id_ujian', $id)->findAll();
        $ids = [];
        foreach ($soal as $s) {
            $ids[] = $s->id;
        }
        try {
            if ($model->delete($ids)) {
                $model->purgeDeleted();
                return redirect()->route('ujian-dosen')->with('message', 'Soal Ujian berhasil dihapus');
            }
            return redirect()->route('ujian-dosen')->with('error', 'Soal Ujian gagal dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }
}
