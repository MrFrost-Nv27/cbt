<?php

namespace App\Controllers\Backend\Master;

use App\Controllers\BaseBackend;
use App\Entities\Entitykuliah;
use App\Models\ModelKuliah;

class Kuliah extends BaseBackend
{
    protected string $model = ModelKuliah::class;
    protected string $entity = Entitykuliah::class;
    protected string $routeSuccess = 'master-kuliah';
}