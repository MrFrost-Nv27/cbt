<?php

namespace App\Controllers\Backend\Master;

use App\Controllers\BaseBackend;
use App\Entities\EntityRuang;
use App\Models\ModelRuang;

class Ruang extends BaseBackend
{
    protected string $model = ModelRuang::class;
    protected string $entity = EntityRuang::class;
    protected string $routeSuccess = 'master-ruang';
}