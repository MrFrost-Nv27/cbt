<?php

namespace App\Controllers\Backend\Master;

use App\Controllers\BaseBackend;
use App\Entities\EntityStudi;
use App\Models\ModelStudi;

class Studi extends BaseBackend
{
    protected string $model = ModelStudi::class;
    protected string $entity = EntityStudi::class;
    protected string $routeSuccess = 'master-studi';
}