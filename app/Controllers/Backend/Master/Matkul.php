<?php

namespace App\Controllers\Backend\Master;

use App\Controllers\BaseBackend;
use App\Entities\EntityMatkul;
use App\Models\ModelMatkul;

class Matkul extends BaseBackend
{
    protected string $model = ModelMatkul::class;
    protected string $entity = EntityMatkul::class;
    protected string $routeSuccess = 'master-matkul';
}