<?php

namespace App\Controllers\Backend\Master;

use App\Controllers\BaseBackend;
use App\Entities\EntityDosen;
use App\Models\ModelDosen;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\Shield\Exceptions\ValidationException;
use CodeIgniter\Shield\Models\UserModel;

class Dosen extends BaseBackend
{
    protected string $model = ModelDosen::class;
    protected string $entity = EntityDosen::class;
    protected string $routeSuccess = 'master-dosen';

    public function beforeSave(array $data)
    {
        $users = model(UserModel::class);
        $rules = $this->getValidationRules();

        if ($data['id'] === null) {
            if (!$this->validateData($data['post'], $rules)) {
                return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
            }
        }

        $user = $data['id'] === null ? new User() : $users->findById(model(ModelDosen::class)->find($data['id'])->id_user);
        $user->fill([
            'username' => $data['post']['username'],
            'email' => $data['post']['email'],
        ]);
        if ($user->id === null) {
            $user->fill([
                'password' => $data['post']['password'],
            ]);
        }

        try {
            if ($user->id === null) {
                $users->save($user);
                $user = $users->findById($users->getInsertID());
                $user->addGroup('dosen', 'user');
                unset($data['post']['password']);
                unset($data['post']['password_confirm']);
            } else {
                if ($user->hasChanged() && !empty($user->username) && !empty($user->email)) {
                    $users->skipValidation()->save($user);
                };
            }

            unset($data['post']['username']);
            unset($data['post']['email']);
            $data['post']['id_user'] = $user->id;

            return $data;
        } catch (ValidationException $e) {
            dd("b");
            return redirect()->back()->withInput()->with('errors', $users->errors());
        }
    }

    protected function getValidationRules(): array
    {
        $registrationUsernameRules = array_merge(
            config('AuthSession')->usernameValidationRules,
            ['is_unique[users.username]']
        );
        $registrationEmailRules = array_merge(
            config('AuthSession')->emailValidationRules,
            ['is_unique[auth_identities.secret]']
        );

        return setting('Validation.registration') ?? [
            'username' => [
                'label' => 'Auth.username',
                'rules' => $registrationUsernameRules,
            ],
            'email' => [
                'label' => 'Auth.email',
                'rules' => $registrationEmailRules,
            ],
            'password' => [
                'label' => 'Auth.password',
                'rules' => 'required|strong_password',
            ],
            'password_confirm' => [
                'label' => 'Auth.passwordConfirm',
                'rules' => 'required|matches[password]',
            ],
        ];
    }
}
