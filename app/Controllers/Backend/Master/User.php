<?php

namespace App\Controllers\Backend\Master;

use App\Controllers\BaseBackend;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\Shield\Entities\User as EntitiesUser;
use CodeIgniter\Shield\Models\UserModel;

class User extends BaseBackend
{
    protected string $model = UserModel::class;
    protected string $entity = EntitiesUser::class;
    protected string $routeSuccess = 'master-user';

    public function save(int $id = null): RedirectResponse
    {
        if (!$this->validate([
            'password' => 'required|min_length[5]',
            'passconf' => 'required|matches[password]',
        ])) {
            return redirect()->back()->with('errors', $this->validator->getErrors());
        }

        $model = model(UserModel::class);
        $pass = $this->request->getPost('password');
        $data = $model->findById($id);
        $data->fill([
            'password' => $pass
        ]);

        try {
            if ($model->save($data)) {
                return redirect()->route($this->routeSuccess)->with('message', 'Data User Berhasil diedit');
            }
            return redirect()->back()->with('errors', $model->errors());
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', $th->getMessage());
        }
    }
}