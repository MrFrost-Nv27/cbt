<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\Shield\Exceptions\LogicException;

abstract class BaseResourceController extends ResourceController
{
    protected $format    = 'json';

    /**
     * @var string|null The entity object row on this resource's data
     */
    protected $entity;

    public function index()
    {
        $dataset = $this->model->findAll();
        if ($dataset) {
            return $this->respond($dataset);
        }
        return $this->fail('Data tidak ditemukan');
    }

    public function show($id = null)
    {
        $data = $this->model->find($id);
        if ($data) {
            return $this->respond($data);
        }
        return $this->fail('Data tidak ditemukan');
    }

    public function create()
    {
        $entity = $this->getEntity();
        $post = $this->request->getPost();
        $data = new $entity($post);

        try {
            if ($this->model->insert($data, false)) {
                return $this->respondCreated([
                    'message' => 'Data baru berhasil ditambahkan'
                ]);
            }
            return $this->fail($this->model->errors());
        } catch (\Throwable $th) {
            return $this->fail($th->getMessage());
        }
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface|string|void
     */
    public function update($id = null)
    {
        $entity = $this->getEntity();
        $post = $this->request->getRawInput();
        $data = new $entity($post);
        try {
            if ($this->model->update($id, $data)) {
                return $this->respond([
                    'message' => 'Data berhasil disimpan'
                ]);
            }
            return $this->fail($this->model->errors());
        } catch (\Throwable $th) {
            return $this->fail($th->getMessage());
        }
    }

    /**
     * Delete the designated resource object from the model
     *
     * @param int|string|null $id
     *
     * @return ResponseInterface|string|void
     */
    public function delete($id = null)
    {
        try {
            if ($this->model->delete($id)) {
                return $this->respond([
                    'message' => 'Data berhasil dihapus'
                ]);
            }
            return $this->fail($this->model->errors());
        } catch (\Throwable $th) {
            return $this->fail($th->getMessage());
        }
    }

    protected function getEntity()
    {
        return $this->entity ?? throw new LogicException('Entity Objek pada resource controller harus di set');
    }
}