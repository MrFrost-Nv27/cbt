<?php

namespace App\Controllers\Gui\Cbt;

use App\Models\ModelMahasiswa;
use App\Models\ModelSoal;
use App\Models\ModelStudi;
use App\Models\ModelUjian;
use CodeIgniter\API\ResponseTrait;

class Room extends BaseCBT
{
    use ResponseTrait;
    public function index(int $id)
    {
        // $session = session();
        // dd($session->get());
        // $session->destroy('cbttoken', 'cbtwaktu');
        $this->addData('ujianUrl', route_to('cbt-room', $id));
        $this->addData('active', 'ujian-room');
        return view("Panel/Ujian/Room/index", $this->pageData);
    }

    public function sign(int $id)
    {
        $cbt = service('cbt');
        $this->addData('active', 'ujian-token');
        $this->addData('attemptUrl', route_to('cbt-room-sign', $id));
        return view("Panel/Ujian/Room/token", $this->pageData);
    }

    public function signAttempt(int $id)
    {
        $session = session();
        $model = model(ModelUjian::class);
        $token = $this->request->getPost('token');
        $token = strtoupper((string) $token);
        $ujian = $model->find($id);
        if ($ujian) {
            if ($ujian->token == $token) {
                $model = model(ModelUjian::class);
                $getSoal = $model->find($id)->soal_pilgan;

                shuffle($getSoal);
                file_put_contents(WRITEPATH . "soal" . DIRECTORY_SEPARATOR . auth()->id() . ".json", json_encode(array_column($getSoal, 'id')));

                $soal = [];
                foreach ($getSoal as $s) {
                    $pilgan = $s->pilihan_ujian;
                    shuffle($pilgan);
                    $soal[] = [
                        'id' => $s->id,
                        'text' => $s->soal,
                        'pilgan' => $pilgan,
                    ];
                }
                $session->set('cbttoken', $id);
                return $this->respond([
                    'url' => base_url(route_to('cbt-room', $id)),
                    'soal' => $soal,
                ]);
            }
            return $this->fail('Token Akses tidak valid');
        }
        return $this->failNotFound('Tidak ditemukan');
    }

    public function waktu(int $id)
    {
        $session = session();
        if ($session->get("cbtwaktu")) {
            return $this->respond([
                'waktu' => $session->get("cbtwaktu"),
            ]);
        } else {
            $model = model(ModelUjian::class);
            $ujian = $model->find($id);
            if ($ujian) {
                $session->set('cbtwaktu', $ujian->deadline->getTimestamp());
                return $this->respond([
                    'waktu' => $ujian->deadline->getTimestamp(),
                ]);
            }
            return $this->failNotFound('Tidak ditemukan');
        }
    }

    public function ujianAttempt(int $id)
    {
        $session = session();
        $model = model(ModelUjian::class);
        $modelSoal = model(ModelSoal::class);
        $modelStudi = model(ModelStudi::class);
        $modelMhs = model(ModelMahasiswa::class);
        $soal = json_decode(file_get_contents(WRITEPATH . "soal" . DIRECTORY_SEPARATOR . auth()->id() . ".json"));

        $answer = $this->request->getPost('answer');
        $ujian = $model->find($id);
        $studi = $modelStudi->where(['id_kuliah' => $ujian->id_kuliah, 'id_mahasiswa' => $modelMhs->where('id_user', auth()->id())->first()->id])->first();

        $jumlahBenar = 0;
        $nilai = 0;
        for ($i = 0; $i < count($answer); $i++) {
            $jumlahBenar += intval($answer[$i] == $modelSoal->find($soal[$i])->pilihan_benar->kode);
        }
        if ($jumlahBenar) {
            $nilai = 100 / count($ujian->soal_pilgan) * $jumlahBenar;
        }
        $studi->{strtolower($ujian->type)} = $nilai;
        try {
            $modelStudi->save($studi);
        } catch (\Throwable $th) {
            return $this->fail($th->getMessage());
        }
        $session->remove('cbttoken', 'cbtwaktu');
        return $this->respond([
            'url' => route_to('ujian-mahasiswa'),
            'nilai' => $nilai,
        ]);
    }
}
