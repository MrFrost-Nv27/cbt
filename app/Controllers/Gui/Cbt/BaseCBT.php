<?php

namespace App\Controllers\Gui\Cbt;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;

class BaseCBT extends BaseController
{
    protected array $pageData = [];
    protected array $dataset = [];

    protected string $title = 'CBT';

    public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
        $this->setInitData();
    }

    private function setInitData(): void
    {
        if ($this->dataset) {
            foreach ($this->dataset as $key => $value) {
                $this->pageData[$key] = $value;
            }
        }

        $this->setTitle($this->title);
    }

    protected function setTitle(String $title): void
    {
        $this->pageData['title'] = $title;
    }

    protected function addData($input, ...$inputExt): void
    {
        if (is_string($input)) {
            if ($inputExt === []) {
                throw new InvalidArgumentException('Data harus berupa pasangan string key dan dynamic value');
            }
            $this->pageData[$input] = $inputExt[0];
        } elseif (is_array($input) && $input !== null && !array_is_list($input)) {
            foreach ($input as $key => $value) {
                $this->pageData[$key] = $value;
            }
        } else {
            throw new InvalidArgumentException('Data harus berupa key value pair atau array asosiatif');
        }
    }
}
