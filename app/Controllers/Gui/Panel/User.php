<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelDosen;
use App\Models\ModelMahasiswa;
use CodeIgniter\Shield\Models\UserModel;

class User extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data User';
    protected string $modelClass = UserModel::class;
    protected string $menuID = 'master-data-user';
    protected string $viewFolder = "User";

    public function index()
    {
        $mahasiswa = model(ModelMahasiswa::class);
        $dosen = model(ModelDosen::class);
        $this->addData('items', [...$mahasiswa->findAll() ?? null, ...$dosen->findAll() ?? null]);
        $this->addData('active', $this->menuID);
        return view("Panel/Master/{$this->viewFolder}/index", $this->pageData);
    }

    public function form(int $id = null)
    {
        $model = model($this->modelClass);
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', $this->menuID);
        return view("Panel/Master/{$this->viewFolder}/form", $this->pageData);
    }
}