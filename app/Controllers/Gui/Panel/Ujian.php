<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelDosen;
use App\Models\ModelMahasiswa;
use App\Models\ModelUjian;

class Ujian extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data Ujian';
    protected string $modelClass = ModelUjian::class;
    protected string $menuID = 'ujian-data';
    protected string $viewFolder = "Ujian";

    public function index()
    {
        $model = model($this->modelClass);
        $this->addData('items', $model->where('done', false)->findAll() ?? null);
        $this->addData('active', $this->menuID);
        return view("Panel/{$this->viewFolder}/Admin/index", $this->pageData);
    }

    public function form(int $id = null)
    {
        $model = model($this->modelClass);
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', $this->menuID);
        return view("Panel/{$this->viewFolder}/Admin/form", $this->pageData);
    }

    public function riwayat()
    {
        $model = model($this->modelClass);
        $this->addData('items', $model->where('done', true)->findAll() ?? null);
        $this->addData('active', 'ujian-riwayat');
        return view("Panel/{$this->viewFolder}/Admin/riwayat", $this->pageData);
    }

    public function dosen()
    {
        $model = model($this->modelClass);
        $modelDosen = model(ModelDosen::class);
        $this->addData('items', $model->findByDosen($modelDosen->where('id_user', auth()->id())->first()->id) ?? null);
        $this->addData('active', 'ujian-dosen');
        return view("Panel/{$this->viewFolder}/Dosen/index", $this->pageData);
    }

    public function upload(int $id)
    {
        $this->addData('active', 'ujian-dosen');
        $this->addData('id', $id);
        return view("Panel/{$this->viewFolder}/Dosen/upload", $this->pageData);
    }

    public function soal(int $id)
    {
        $model = model($this->modelClass);
        $this->addData('active', 'ujian-dosen');
        $this->addData('item', $model->find($id));
        return view("Panel/{$this->viewFolder}/Dosen/soal", $this->pageData);
    }

    public function mahasiswa()
    {
        $model = model($this->modelClass);
        $modelMhs = model(ModelMahasiswa::class);
        $this->addData('items', $model->findByMahasiswa($modelMhs->where('id_user', auth()->id())->first()->id) ?? null);
        $this->addData('active', 'ujian-list');
        return view("Panel/{$this->viewFolder}/Mahasiswa/index", $this->pageData);
    }
}