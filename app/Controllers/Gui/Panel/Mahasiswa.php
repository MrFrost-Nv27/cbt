<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelMahasiswa;

class Mahasiswa extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data Mahasiswa';
    protected string $modelClass = ModelMahasiswa::class;
    protected string $menuID = 'master-data-mahasiswa';
    protected string $viewFolder = "Mahasiswa";
}