<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelStudi;

class Studi extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data Studi';
    protected string $modelClass = ModelStudi::class;
    protected string $menuID = 'master-kampus-studi';
    protected string $viewFolder = "Studi";
}