<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelDosen;
use App\Models\ModelKuliah;
use App\Models\ModelMahasiswa;
use App\Models\ModelMatkul;
use App\Models\ModelRuang;

class Main extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Dashboard';

    public function index()
    {
        $this->addData('active', 'dashboard');
        return view('Panel/dashboard', $this->pageData);
    }

    public function dosen()
    {
        /** @var ModelDosen $model */
        $model = model('ModelDosen');
        $this->addData('items', $model->findAll() ?? null);
        $this->addData('active', 'master-data-dosen');
        return view('Panel/Master/dosen', $this->pageData);
    }

    public function dosenForm(int $id = null)
    {
        /** @var ModelDosen $model */
        $model = model('ModelDosen');
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', 'master-data-dosen');
        return view('Panel/Master/dosen-form', $this->pageData);
    }

    public function mahasiswa()
    {
        /** @var ModelMahasiswa $model */
        $model = model('ModelMahasiswa');
        $this->addData('items', $model->findAll() ?? null);
        $this->addData('active', 'master-data-mahasiswa');
        return view('Panel/Master/mahasiswa', $this->pageData);
    }

    public function mahasiswaForm(int $id = null)
    {
        /** @var ModelMahasiswa $model */
        $model = model('ModelMahasiswa');
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', 'master-data-mahasiswa');
        return view('Panel/Master/mahasiswa-form', $this->pageData);
    }

    public function matkul()
    {
        /** @var ModelMatkul $model */
        $model = model('ModelMatkul');
        $this->addData('items', $model->findAll() ?? null);
        $this->addData('active', 'master-data-matkul');
        return view('Panel/Master/matkul', $this->pageData);
    }

    public function matkulForm(int $id = null)
    {
        /** @var ModelMatkul $model */
        $model = model('ModelMatkul');
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', 'master-data-matkul');
        return view('Panel/Master/matkul-form', $this->pageData);
    }

    public function ruang()
    {
        /** @var ModelRuang $model */
        $model = model('ModelRuang');
        $this->addData('items', $model->findAll() ?? null);
        $this->addData('active', 'master-data-ruang');
        return view('Panel/Master/ruang', $this->pageData);
    }

    public function ruangForm(int $id = null)
    {
        /** @var ModelRuang $model */
        $model = model('ModelRuang');
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', 'master-data-ruang');
        return view('Panel/Master/ruang-form', $this->pageData);
    }

    public function kuliah()
    {
        /** @var ModelKuliah $model */
        $model = model('ModelKuliah');
        $this->addData('items', $model->findAll() ?? null);
        $this->addData('active', 'master-data-kuliah');
        return view('Panel/Master/kuliah', $this->pageData);
    }

    public function kuliahForm(int $id = null)
    {
        /** @var ModelKuliah $model */
        $model = model('ModelKuliah');
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', 'master-data-kuliah');
        return view('Panel/Master/kuliah-form', $this->pageData);
    }

    public function studi()
    {
    }

    public function studiForm(int $id = null)
    {
    }
}