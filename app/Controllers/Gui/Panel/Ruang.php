<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelRuang;

class Ruang extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data Ruang';
    protected string $modelClass = ModelRuang::class;
    protected string $menuID = 'master-kampus-ruang';
    protected string $viewFolder = "Ruang";
}