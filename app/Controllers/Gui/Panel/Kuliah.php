<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelKuliah;

class Kuliah extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data Kuliah';
    protected string $modelClass = ModelKuliah::class;
    protected string $menuID = 'master-kampus-kuliah';
    protected string $viewFolder = "Kuliah";
}