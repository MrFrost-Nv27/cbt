<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelMatkul;

class Matkul extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data Mata kuliah';
    protected string $modelClass = ModelMatkul::class;
    protected string $menuID = 'master-kampus-matkul';
    protected string $viewFolder = "Matkul";
}