<?php

namespace App\Controllers\Gui\Panel;

use App\Controllers\BaseDashboard;
use App\Models\ModelDosen;

class Dosen extends BaseDashboard
{
    protected array $dataset = [];
    protected string $title = 'Manajemen Data Dosen';
    protected string $modelClass = ModelDosen::class;
    protected string $menuID = 'master-data-dosen';
    protected string $viewFolder = "Dosen";
}