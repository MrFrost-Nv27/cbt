<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Shield\Database\Migrations\CreateAuthTables;
use Exception;
use Throwable;

class Migrate extends Controller
{
    public function index()
    {
        $dbname = env('database.default.database');
        $db = \Config\Database::connect();

        try {
            $migrate = \Config\Services::migrations();
            $seeder = \Config\Database::seeder();
            $forge = \Config\Database::forge();

            $tables = $db->listTables();

            foreach ($tables as $table) {
                $forge->dropTable($table, true);
            }

            $migrate->setNamespace('CodeIgniter\Settings')->latest();
            $migrate->setNamespace('CodeIgniter\Shield')->latest();
            $migrate->setNamespace('App')->latest();

            $seeder->call('CBT');
        } catch (Throwable $e) {
            throw new Exception($e->getMessage());
        }

        return 'berhasil migrate database, <a href="' . base_url() . '">Kembali</a>';
    }
}