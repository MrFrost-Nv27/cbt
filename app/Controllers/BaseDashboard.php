<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Shield\Exceptions\InvalidArgumentException;
use Psr\Log\LoggerInterface;

class BaseDashboard extends BaseController
{
    protected array $pageData = [];
    protected array $dataset = [];

    protected string $title = 'Judul';
    protected string $modelClass;
    protected string $menuID;
    protected string $viewFolder;

    public function initController(
        RequestInterface $request,
        ResponseInterface $response,
        LoggerInterface $logger
    ) {
        parent::initController($request, $response, $logger);
        $this->setInitData();
        $this->setInitMenu();
    }

    public function index()
    {
        $model = model($this->modelClass);
        $this->addData('items', $model->findAll() ?? null);
        $this->addData('active', $this->menuID);
        return view("Panel/Master/{$this->viewFolder}/index", $this->pageData);
    }

    public function form(int $id = null)
    {
        $model = model($this->modelClass);
        $this->addData('item', $id ? $model->find($id) : null);
        $this->addData('active', $this->menuID);
        return view("Panel/Master/{$this->viewFolder}/form", $this->pageData);
    }

    private function setInitData(): void
    {
        if ($this->dataset) {
            foreach ($this->dataset as $key => $value) {
                $this->pageData[$key] = $value;
            }
        }

        $this->setTitle($this->title);
    }

    protected function setTitle(String $title): void
    {
        $this->pageData['title'] = $title;
    }

    protected function addData($input, ...$inputExt): void
    {
        if (is_string($input)) {
            if ($inputExt === []) {
                throw new InvalidArgumentException('Data harus berupa pasangan string key dan dynamic value');
            }
            $this->pageData[$input] = $inputExt[0];
        } elseif (is_array($input) && $input !== null && !array_is_list($input)) {
            foreach ($input as $key => $value) {
                $this->pageData[$key] = $value;
            }
        } else {
            throw new InvalidArgumentException('Data harus berupa key value pair atau array asosiatif');
        }
    }

    protected function addMenu(array $listMenu, string $header = null): void
    {
        $menu = [
            'header' => $header ? true : false,
            'title' => $header ?? null,
            'listmenu' => $listMenu ?? [$this->_addMenu()],
        ];

        $this->pageData['menu'][] = $menu;
    }

    private function _addMenu(array $option = [], bool $force = false)
    {
        $result = [
            'sub' => false,
            'id' => $option['id'] ?? random_string(),
            'link' => $option['link'] ?? '#',
            'icon' => $option['icon'] ?? 'grid-large',
            'label' => $option['label'] ?? 'Label',
        ];

        if ($force) {
            $this->pageData['menu'][] = $result;
            return;
        }
        return $result;
    }

    private function _addMenuWithSub(array $mainOpt = [], array $listSub = [], bool $force = false)
    {
        $result = [
            'sub' => true,
            'id' => $mainOpt['id'] ?? random_string(),
            'icon' => $mainOpt['icon'] ?? 'grid-large',
            'label' => $mainOpt['label'] ?? 'Label',
            'listsub' => $listSub ?? [$this->_addMenu()],
        ];

        if ($force) {
            $this->pageData['menu'][] = $result;
            return;
        }
        return $result;
    }

    private function setInitMenu(): void
    {
        $this->addMenu([
            $this->_addMenu([
                'id' => 'dashboard',
                'label' => 'Dashboard',
                'icon' => 'grid-large',
                'link' => base_url(),
            ]),
        ]);
        if (auth()->user()->inGroup('admin')) {
            $this->addMenu([
                $this->_addMenuWithSub(
                    [
                        'id' => 'master-data',
                        'icon' => $mainOpt['icon'] ?? 'account-group-outline',
                        'label' => $mainOpt['label'] ?? 'Master Data',
                    ],
                    [
                        $this->_addMenu([
                            'id' => 'master-data-dosen',
                            'label' => 'Data Dosen',
                            'link' => route_to('master-dosen'),
                        ]),
                        $this->_addMenu([
                            'id' => 'master-data-mahasiswa',
                            'label' => 'Data Mahasiswa',
                            'link' => route_to('master-mahasiswa'),
                        ]),
                        $this->_addMenu([
                            'id' => 'master-data-user',
                            'label' => 'Data User',
                            'link' => route_to('master-user'),
                        ]),
                    ]
                ),
            ]);
            $this->addMenu([
                $this->_addMenuWithSub(
                    [
                        'id' => 'master-kampus',
                        'icon' => $mainOpt['icon'] ?? 'school-outline',
                        'label' => $mainOpt['label'] ?? 'Akademik',
                    ],
                    [
                        $this->_addMenu([
                            'id' => 'master-kampus-matkul',
                            'label' => 'Data Mata Kuliah',
                            'link' => route_to('master-matkul'),
                        ]),
                        $this->_addMenu([
                            'id' => 'master-kampus-ruang',
                            'label' => 'Data Ruang',
                            'link' => route_to('master-ruang'),
                        ]),
                        $this->_addMenu([
                            'id' => 'master-kampus-kuliah',
                            'label' => 'Data Kuliah',
                            'link' => route_to('master-kuliah'),
                        ]),
                        $this->_addMenu([
                            'id' => 'master-kampus-studi',
                            'label' => 'Data Studi',
                            'link' => route_to('master-studi'),
                        ]),
                    ]
                ),
            ]);
            $this->addMenu([
                $this->_addMenuWithSub(
                    [
                        'id' => 'ujian',
                        'icon' => $mainOpt['icon'] ?? 'book-open-page-variant-outline',
                        'label' => $mainOpt['label'] ?? 'Master Ujian',
                    ],
                    [
                        $this->_addMenu([
                            'id' => 'ujian-data',
                            'label' => 'Data Ujian',
                            'link' => route_to('ujian-data'),
                        ]),
                        $this->_addMenu([
                            'id' => 'ujian-riwayat',
                            'label' => 'Riwayat Ujian',
                            'link' => route_to('ujian-riwayat'),
                        ]),
                    ]
                ),
            ]);
        }
        if (auth()->user()->inGroup('dosen')) {
            $this->addMenu([
                $this->_addMenu([
                    'id' => 'ujian-dosen',
                    'label' => 'List Ujian',
                    'icon' => 'book',
                    'link' => route_to('ujian-dosen'),
                ]),
            ]);
        }
        if (auth()->user()->inGroup('mahasiswa')) {
            $this->addMenu([
                $this->_addMenu([
                    'id' => 'ujian-list',
                    'label' => 'List Ujian',
                    'icon' => 'mdi mdi-format-list-checks',
                    'link' => route_to('ujian-mahasiswa'),
                ]),
            ]);
        }
    }
}
