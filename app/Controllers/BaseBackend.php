<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\RedirectResponse;
use LogicException;

class BaseBackend extends BaseController
{
    protected string $routeSuccess;
    protected string $routeFail;
    protected string $model;
    protected string $entity;

    public function save(int $id = null): RedirectResponse
    {
        $post = $this->request->getPost();
        $entity = $this->getEntity();
        $model = model($this->getModel());

        $beforeData = $this->beforeSave(['post' => $post, 'id' => $id]);

        if ($beforeData instanceof RedirectResponse) {
            return $beforeData;
        } elseif (is_array($beforeData) && !empty($beforeData)) {
            $post = $beforeData['post'];
        }

        $data = $id ? $model->find($id) : new $entity();
        $data->fill($post);

        if (!$this->validateData($data->toRawArray(), $model->getValidationRules())) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        try {
            if ($model->skipValidation()->save($data)) {
                $afterData = $this->afterSave(['data' => $data, 'insertID' => $model->getInsertID()]);

                if ($afterData instanceof RedirectResponse) {
                    return $afterData;
                } elseif (is_array($afterData) && !empty($afterData)) {
                    $data = array_merge($afterData['data'], $data);
                }
                $actionMsg = $data->id ? "Data berhasil di simpan" : "Data baru berhasil Ditambahkan";
                return redirect()->route($this->routeSuccess)->with('message', $actionMsg);
            }
            return redirect()->back()->withInput()->with('errors', $model->errors());
        } catch (\Throwable $th) {
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }
    }

    public function delete(int $id = null)
    {
        $model = model($this->getModel());

        $beforeData = $this->beforeDelete(['id' => $id]);

        try {
            if ($model->delete($id)) {
                $afterData = $this->afterDelete(['id' => $id]);
                return redirect()->route($this->routeSuccess)->with('message', 'Data Berhasil dihapus');
            }
            return redirect()->back()->with('errors', $model->errors());
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', $th->getMessage());
        }
    }

    protected function getModel()
    {
        return $this->model ?? throw new LogicException('Model harus di set');
    }

    protected function getEntity()
    {
        return $this->entity ?? throw new LogicException('Entity harus di set');
    }

    protected function beforeSave(array $data)
    {
    }

    protected function afterSave(array $data)
    {
    }

    protected function beforeDelete(array $data)
    {
    }

    protected function afterDelete(array $data)
    {
    }
}