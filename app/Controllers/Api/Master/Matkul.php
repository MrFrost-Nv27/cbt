<?php

namespace App\Controllers\Api\Master;

use App\Controllers\BaseResourceController;
use App\Entities\EntityMatkul;
use App\Models\ModelMatkul;

class Matkul extends BaseResourceController
{
    protected $modelName = ModelMatkul::class;
    protected $entity = EntityMatkul::class;
}