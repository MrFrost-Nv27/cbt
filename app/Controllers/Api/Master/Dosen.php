<?php

namespace App\Controllers\Api\Master;

use App\Controllers\BaseResourceController;
use App\Entities\EntityDosen;
use App\Models\ModelDosen;

class Dosen extends BaseResourceController
{
    protected $modelName = ModelDosen::class;
    protected $entity = EntityDosen::class;
}
