<?php

namespace App\Controllers\Api\Master;

use App\Controllers\BaseResourceController;
use App\Entities\EntityRuang;
use App\Models\ModelRuang;

class Ruang extends BaseResourceController
{
    protected $modelName = ModelRuang::class;
    protected $entity = EntityRuang::class;
}