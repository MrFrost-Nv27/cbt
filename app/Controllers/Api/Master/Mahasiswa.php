<?php

namespace App\Controllers\Api\Master;

use App\Controllers\BaseResourceController;
use App\Entities\EntityMahasiswa;
use App\Models\ModelMahasiswa;

class Mahasiswa extends BaseResourceController
{
    protected $modelName = ModelMahasiswa::class;
    protected $entity = EntityMahasiswa::class;
}