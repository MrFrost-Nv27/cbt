<?php

namespace App\Models;

use App\Entities\EntityMahasiswa;

class ModelMahasiswa extends BaseModel
{
    protected $table            = 'cbt_mahasiswa';
    protected $returnType       = EntityMahasiswa::class;
    protected $allowedFields    = [
        'id_user',
        'nim',
        'nama',
        'alamat',
    ];
    protected $validationRules = [
        'id_user' => 'required|numeric',
        'nim' => 'required|numeric',
        'nama' => 'required',
    ];

    public function inputKuliah(int $idMatkul)
    {
        $kuliah = model(ModelStudi::class)->where('id_matkul', $idMatkul)->findAll() ?? null;
        if ($kuliah) {
            $except = [];
            foreach ($kuliah as $item) {
                $except[] = $item->id_mahasiswa;
            }
            return $this->builder()
                ->whereNotIn('id', $except)
                ->get()
                ->getResult($this->returnType);
        }
        return $this->builder()
            ->get()
            ->getResult($this->returnType);
    }
}