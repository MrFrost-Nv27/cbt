<?php

namespace App\Models;

use App\Entities\EntityUjian;
use CodeIgniter\Database\BaseBuilder;

class ModelUjian extends BaseModel
{
    protected $table            = 'cbt_ujian';
    protected $returnType       = EntityUjian::class;
    protected $allowedFields    = [
        'id_kuliah',
        'id_ruang',
        'waktu',
        'durasi',
        'type',
        'token',
        'done',
    ];
    protected $validationRules = [
        'id_kuliah' => 'required|numeric',
        'id_ruang'  => 'required|numeric',
        'waktu'     => 'required',
        'durasi'    => 'required|numeric',
        'type'      => 'required',
    ];
    protected $afterDelete = ['clearSoal'];

    public function findByDosen(int $id_dosen)
    {
        $kuliah = model(ModelKuliah::class)->where('id_dosen', $id_dosen)->findAll();
        if ($kuliah) {
            return $this->whereIn('id_kuliah', static fn (BaseBuilder $builder) => $builder->select('id')->from('cbt_kuliah')->where('id_dosen', $id_dosen))->findAll();
        }
        return null;
    }

    public function findByMahasiswa(int $id_mahasiswa)
    {
        $studi = model(ModelStudi::class)->where('id_mahasiswa', $id_mahasiswa)->findAll();
        if ($studi) {
            return $this->whereIn('id_kuliah', static fn (BaseBuilder $builder) => $builder->select('id_kuliah')->from('cbt_studi')->where('id_mahasiswa', $id_mahasiswa))->findAll();
        }
        return null;
    }

    public function clearSoal(array $data)
    {
        $model = model(ModelSoal::class);
        foreach ($data['id'] as $idUjian) {
            $soal = $model->where('id_ujian', $idUjian)->findAll();
            if ($soal) {
                foreach ($soal as $s) {
                    $model->delete($s->id);
                }
            }
        }
        $model->purgeDeleted();
    }
}
