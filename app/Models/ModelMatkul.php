<?php

namespace App\Models;

use App\Entities\EntityMatkul;

class ModelMatkul extends BaseModel
{
    protected $table            = 'cbt_matkul';
    protected $returnType       = EntityMatkul::class;
    protected $allowedFields    = [
        'kode',
        'nama',
        'sks',
    ];
    protected $validationRules = [
        'kode' => 'required',
        'nama' => 'required',
        'sks' => 'required|numeric',
    ];

    public function inputKuliah()
    {
        $kuliah = model(ModelKuliah::class)->findAll() ?? null;
        if ($kuliah) {
            $except = [];
            foreach ($kuliah as $item) {
                $except[] = $item->id_matkul;
            }
            return $this->builder()
                ->whereNotIn('id', $except)
                ->get()
                ->getResult($this->returnType);
        }
        return $this->builder()
            ->get()
            ->getResult($this->returnType);
    }
}