<?php

namespace App\Models;

use App\Entities\EntityPilgan;

class ModelPilgan extends BaseModel
{
    protected $table            = 'cbt_pilgan';
    protected $returnType       = EntityPilgan::class;
    protected $allowedFields    = [
        'id_ujian',
        'id_soal',
        'kode',
        'text',
        'benar',
    ];
    protected $validationRules = [
        'id_ujian' => 'required|numeric',
        'id_soal'  => 'required|numeric',
        'kode'     => 'required',
        'text'     => 'required',
    ];
}