<?php

namespace App\Models;

use App\Entities\EntityRuang;

class ModelRuang extends BaseModel
{
    protected $table            = 'cbt_ruang';
    protected $returnType       = EntityRuang::class;
    protected $allowedFields    = [
        'kode',
        'nama',
        'kapasitas',
    ];
    protected $validationRules = [
        'kode' => 'required',
        'nama' => 'required',
        'kapasitas' => 'numeric',
    ];
}