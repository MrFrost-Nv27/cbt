<?php

namespace App\Models;

use App\Entities\EntitySoal;

class ModelSoal extends BaseModel
{
    protected $table            = 'cbt_soal';
    protected $returnType       = EntitySoal::class;
    protected $allowedFields    = [
        'id_ujian',
        'soal',
        'nilai',
        'type',
    ];
    protected $validationRules = [
        'id_ujian' => 'required|numeric',
        'soal'     => 'required',
    ];

    protected $allowCallbacks = true;
    protected $beforeDelete = ['clearPilgan'];

    public function byUjianId(int $id)
    {
        return $this->where('id_ujian', $id)->findAll();
    }

    public function clearPilgan(array $data)
    {
        $pilgan = model(ModelPilgan::class);
        foreach ($data['id'] as $idSoal) {
            $soal = $this->find($idSoal);
            if ($soal) {
                if ($soal->type == 1) {
                    $pilgan->where(['id_ujian' => $soal->id_ujian, 'id_soal' => $soal->id])->delete();
                }
            }
        }
        $pilgan->purgeDeleted();
    }
}