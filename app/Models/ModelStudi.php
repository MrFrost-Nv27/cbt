<?php

namespace App\Models;

use App\Entities\EntityStudi;
use CodeIgniter\Exceptions\ModelException;

class ModelStudi extends BaseModel
{
    protected $table            = 'cbt_studi';
    protected $returnType       = EntityStudi::class;
    protected $allowedFields    = [
        'id_kuliah',
        'id_mahasiswa',
        'uts',
        'uas',
    ];
    protected $validationRules = [
        'id_kuliah'    => 'required|numeric',
        'id_mahasiswa' => 'required|numeric',
    ];

    protected $beforeInsert = ['cekDuplikasi'];

    protected function cekDuplikasi(array $data)
    {
        $exist = $this->where(['id_kuliah' => $data['data']['id_kuliah'], 'id_mahasiswa' => $data['data']['id_mahasiswa']])->findAll();
        if (empty($exist)) {
            return $data;
        }
        throw new ModelException('Mahasiswa Tersebut telah mengambil kuliah terkait');
    }
}