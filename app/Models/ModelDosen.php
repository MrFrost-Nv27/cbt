<?php

namespace App\Models;

use App\Entities\EntityDosen;

class ModelDosen extends BaseModel
{
    protected $table            = 'cbt_dosen';
    protected $returnType       = EntityDosen::class;
    protected $allowedFields    = [
        'id_user',
        'nidn',
        'nama',
        'alamat',
    ];
    protected $validationRules = [
        'id_user' => 'required|numeric',
        'nidn' => 'required|numeric|is_unique[cbt_dosen.nidn]',
        'nama' => 'required',
    ];
    protected $validationMessages = [
        'nidn' => [
            'is_unique' => 'NIDN sudah terpakai atau sama dengan user lain',
        ],
    ];
}
