<?php

namespace App\Models;

use App\Entities\Entitykuliah;

class ModelKuliah extends BaseModel
{
    protected $table            = 'cbt_kuliah';
    protected $returnType       = Entitykuliah::class;
    protected $allowedFields    = [
        'id_matkul',
        'id_dosen',
        'id_ruang',
    ];
    protected $validationRules = [
        'id_matkul' => 'required|numeric',
        'id_dosen'  => 'required|numeric',
        'id_ruang'  => 'required|numeric',
    ];
}