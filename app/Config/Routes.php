<?php

namespace Config;

use App\Controllers\Gui\Panel\Main;
use App\Controllers\SourceController;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', [Main::class, 'index'],);

$routes->get('/script/(:any)', [SourceController::class, 'script'],);
$routes->get('/style/(:any)', [SourceController::class, 'style'],);

service('auth')->routes($routes);
service('cbt')->routes($routes);

$routes->group('backend', static function ($routes) {
    $routes->group('master', ['filter' => 'group:admin,superadmin'], static function ($routes) {
        $routes->group('dosen', ['namespace' => 'App\Controllers\Backend\Master'], static function ($routes) {
            $routes->post('add', 'Dosen::save', ['as' => 'backend-dosen-add']);
            $routes->post('edit/(:num)', 'Dosen::save/$1', ['as' => 'backend-dosen-edit']);
            $routes->get('delete/(:num)', 'Dosen::delete/$1', ['as' => 'backend-dosen-delete']);
        });
        $routes->group('mahasiswa', ['namespace' => 'App\Controllers\Backend\Master'], static function ($routes) {
            $routes->post('add', 'Mahasiswa::save', ['as' => 'backend-mahasiswa-add']);
            $routes->post('edit/(:num)', 'Mahasiswa::save/$1', ['as' => 'backend-mahasiswa-edit']);
            $routes->get('delete/(:num)', 'Mahasiswa::delete/$1', ['as' => 'backend-mahasiswa-delete']);
        });
        $routes->group('matkul', ['namespace' => 'App\Controllers\Backend\Master'], static function ($routes) {
            $routes->post('add', 'Matkul::save', ['as' => 'backend-matkul-add']);
            $routes->post('edit/(:num)', 'Matkul::save/$1', ['as' => 'backend-matkul-edit']);
            $routes->get('delete/(:num)', 'Matkul::delete/$1', ['as' => 'backend-matkul-delete']);
        });
        $routes->group('ruang', ['namespace' => 'App\Controllers\Backend\Master'], static function ($routes) {
            $routes->post('add', 'Ruang::save', ['as' => 'backend-ruang-add']);
            $routes->post('edit/(:num)', 'Ruang::save/$1', ['as' => 'backend-ruang-edit']);
            $routes->get('delete/(:num)', 'Ruang::delete/$1', ['as' => 'backend-ruang-delete']);
        });
        $routes->group('kuliah', ['namespace' => 'App\Controllers\Backend\Master'], static function ($routes) {
            $routes->post('add', 'Kuliah::save', ['as' => 'backend-kuliah-add']);
            $routes->post('edit/(:num)', 'Kuliah::save/$1', ['as' => 'backend-kuliah-edit']);
            $routes->get('delete/(:num)', 'Kuliah::delete/$1', ['as' => 'backend-kuliah-delete']);
        });
        $routes->group('studi', ['namespace' => 'App\Controllers\Backend\Master'], static function ($routes) {
            $routes->post('add', 'Studi::save', ['as' => 'backend-studi-add']);
            $routes->post('edit/(:num)', 'Studi::save/$1', ['as' => 'backend-studi-edit']);
            $routes->get('delete/(:num)', 'Studi::delete/$1', ['as' => 'backend-studi-delete']);
        });
        $routes->group('user', ['namespace' => 'App\Controllers\Backend\Master'], static function ($routes) {
            $routes->post('edit/(:num)', 'User::save/$1', ['as' => 'backend-user-edit']);
        });
    });
    $routes->group('ujian', ['namespace' => 'App\Controllers\Backend\Ujian'], static function ($routes) {
        $routes->post('add', 'Master::save', ['as' => 'backend-ujian-add']);
        $routes->post('edit/(:num)', 'Master::save/$1', ['as' => 'backend-ujian-edit']);
        $routes->get('delete/(:num)', 'Master::delete/$1', ['as' => 'ujian-delete']);
        $routes->get('done/(:num)', 'Master::setDone/$1', ['as' => 'ujian-done']);
        $routes->post('template', 'Master::template', ['as' => 'backend-ujian-template']);
        $routes->post('upload/(:num)', 'Master::upload/$1', ['as' => 'backend-ujian-upload']);
        $routes->get('reset/(:num)', 'Master::reset/$1', ['as' => 'backend-ujian-reset']);
    });
});

$routes->group('api', static function ($routes) {
    $routes->group('master', ['namespace' => 'App\Controllers\Api\Master'], static function ($routes) {
        $routes->resource('dosen');
        $routes->resource('mahasiswa');
        $routes->resource('matkul');
        $routes->resource('ruang');
    });
});

$routes->group('master', ['namespace' => 'App\Controllers\Gui\Panel', 'filter' => 'group:admin,superadmin'], static function ($routes) {
    $routes->group('dosen', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
        $routes->get('', 'Dosen::index', ['as' => 'master-dosen']);
        $routes->get('add', 'Dosen::form', ['as' => 'master-dosen-add']);
        $routes->get('edit/(:num)', 'Dosen::form/$1', ['as' => 'master-dosen-edit']);
    });
    $routes->group('mahasiswa', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
        $routes->get('', 'Mahasiswa::index', ['as' => 'master-mahasiswa']);
        $routes->get('add', 'Mahasiswa::form', ['as' => 'master-mahasiswa-add']);
        $routes->get('edit/(:num)', 'Mahasiswa::form/$1', ['as' => 'master-mahasiswa-edit']);
    });
    $routes->group('matkul', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
        $routes->get('', 'Matkul::index', ['as' => 'master-matkul']);
        $routes->get('add', 'Matkul::form', ['as' => 'master-matkul-add']);
        $routes->get('edit/(:num)', 'Matkul::form/$1', ['as' => 'master-matkul-edit']);
    });
    $routes->group('ruang', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
        $routes->get('', 'Ruang::index', ['as' => 'master-ruang']);
        $routes->get('add', 'Ruang::form', ['as' => 'master-ruang-add']);
        $routes->get('edit/(:num)', 'Ruang::form/$1', ['as' => 'master-ruang-edit']);
    });
    $routes->group('kuliah', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
        $routes->get('', 'Kuliah::index', ['as' => 'master-kuliah']);
        $routes->get('add', 'Kuliah::form', ['as' => 'master-kuliah-add']);
        $routes->get('edit/(:num)', 'Kuliah::form/$1', ['as' => 'master-kuliah-edit']);
    });
    $routes->group('studi', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
        $routes->get('', 'Studi::index', ['as' => 'master-studi']);
        $routes->get('add', 'Studi::form', ['as' => 'master-studi-add']);
        $routes->get('edit/(:num)', 'Studi::form/$1', ['as' => 'master-studi-edit']);
    });
    $routes->group('user', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
        $routes->get('', 'User::index', ['as' => 'master-user']);
        $routes->get('edit/(:num)', 'User::form/$1', ['as' => 'master-user-edit']);
    });
});

$routes->group('ujian', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
    $routes->get('data', 'Ujian::index', ['as' => 'ujian-data']);
    $routes->get('add', 'Ujian::form', ['as' => 'ujian-add']);
    $routes->get('edit/(:num)', 'Ujian::form/$1', ['as' => 'ujian-edit']);
    $routes->get('riwayat', 'Ujian::riwayat', ['as' => 'ujian-riwayat']);
    $routes->group('room', ['filter' => 'group:mahasiswa', 'namespace' => 'App\Controllers\Gui\Cbt'], static function ($routes) {
        $routes->get('(:num)/sign', 'Room::sign/$1', ['filter' => 'cbttoken:sign', 'as' => 'cbt-room-sign']);
        $routes->get('(:num)/waktu', 'Room::waktu/$1', ['as' => 'cbt-room-waktu']);
        $routes->post('(:num)/sign', 'Room::signAttempt/$1', ['as' => 'cbt-room-sign']);
        $routes->get('(:num)', 'Room::index/$1', ['filter' => 'cbttoken:room', 'as' => 'cbt-room']);
        $routes->post('(:num)/done', 'Room::ujianAttempt/$1', ['as' => 'cbt-room-done']);
    });
});
$routes->group('tes', ['namespace' => 'App\Controllers\Gui\Panel'], static function ($routes) {
    $routes->get('dosen', 'Ujian::dosen', ['as' => 'ujian-dosen']);
    $routes->get('upload/(:num)', 'Ujian::upload/$1', ['as' => 'ujian-upload']);
    $routes->get('soal/(:num)', 'Ujian::soal/$1', ['as' => 'ujian-soal']);
    $routes->get('mahasiswa', 'Ujian::mahasiswa', ['as' => 'ujian-mahasiswa']);
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
