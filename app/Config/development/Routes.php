<?php

namespace Config\Development;

use App\Controllers\Migrate;
use CodeIgniter\Router\RouteCollection;

/** @var RouteCollection $routes */

$routes->get('migrate', [Migrate::class, 'index']);