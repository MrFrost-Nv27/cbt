<?php

namespace Config\Production;

use CodeIgniter\Router\RouteCollection;

/** @var RouteCollection $routes */

$routes->addRedirect('migrate', base_url());