<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use CodeIgniter\Database\BaseConnection;

class CBT extends Migration
{
    public function up()
    {
        $defId = ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true];
        $defInteger = ['type' => 'int', 'constraint' => 11, 'unsigned' => true];
        $defIntegerNull = ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'null' => true];
        $defBoolTrue = ['type' => 'boolean', 'default' => true];
        $defBoolFalse = ['type' => 'boolean', 'default' => false];
        $medVarchar = ['type' => 'varchar', 'constraint' => 64, 'null' => true];
        $defText = ['type' => 'text', 'null' => true];
        $defDate = ['type' => 'datetime', 'null' => true];

        $this->forge->addField([
            'id'         => $defId,
            'id_user'    => $defInteger,
            'nim'        => $defInteger,
            'nama'       => $medVarchar,
            'alamat'     => $defText,
            'created_at' => $defDate,
            'updated_at' => $defDate,
            'deleted_at' => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_mahasiswa');

        $this->forge->addField([
            'id'         => $defId,
            'id_user'    => $defInteger,
            'nidn'       => $defInteger,
            'nama'       => $medVarchar,
            'alamat'     => $defText,
            'created_at' => $defDate,
            'updated_at' => $defDate,
            'deleted_at' => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addUniqueKey('nidn');
        $this->forge->createTable('cbt_dosen');

        $this->forge->addField([
            'id'         => $defId,
            'kode'       => $medVarchar,
            'nama'       => $medVarchar,
            'sks'        => $defInteger,
            'created_at' => $defDate,
            'updated_at' => $defDate,
            'deleted_at' => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_matkul');

        $this->forge->addField([
            'id'            => $defId,
            'id_matkul'     => $defInteger,
            'id_dosen'      => $defInteger,
            'id_ruang'      => $defInteger,
            'created_at'    => $defDate,
            'updated_at'    => $defDate,
            'deleted_at'    => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_kuliah');

        $this->forge->addField([
            'id'            => $defId,
            'id_kuliah'     => $defInteger,
            'id_mahasiswa'  => $defInteger,
            'uts'           => $defIntegerNull,
            'uas'           => $defIntegerNull,
            'created_at'    => $defDate,
            'updated_at'    => $defDate,
            'deleted_at'    => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_studi');

        $this->forge->addField([
            'id'         => $defId,
            'kode'       => $medVarchar,
            'nama'       => $medVarchar,
            'kapasitas'  => $defInteger,
            'created_at' => $defDate,
            'updated_at' => $defDate,
            'deleted_at' => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_ruang');

        $this->forge->addField([
            'id'         => $defId,
            'id_kuliah'  => $defInteger,
            'id_ruang'   => $defInteger,
            'waktu'      => $defDate,
            'durasi'     => $defInteger,
            'type'       => $medVarchar,
            'token'      => $medVarchar,
            'done'       => $defBoolFalse,
            'created_at' => $defDate,
            'updated_at' => $defDate,
            'deleted_at' => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_ujian');

        $this->forge->addField([
            'id'         => $defId,
            'id_ujian'   => $defInteger,
            'soal'       => $defText,
            'nilai'      => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'null' => true],
            'type'       => ['type' => 'int', 'constraint' => 2, 'unsigned' => true, 'default' => 0],
            'created_at' => $defDate,
            'updated_at' => $defDate,
            'deleted_at' => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_soal');

        $this->forge->addField([
            'id'         => $defId,
            'id_ujian'   => $defInteger,
            'id_soal'    => $defInteger,
            'kode'       => $medVarchar,
            'text'       => $defText,
            'benar'      => $defBoolFalse,
            'created_at' => $defDate,
            'updated_at' => $defDate,
            'deleted_at' => $defDate,
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('cbt_pilgan');
    }

    public function down()
    {
        /** @var BaseConnection $db */
        $db = $this->db;
        $db->disableForeignKeyChecks();

        $this->forge->dropTable('cbt_mahasiswa', true);
        $this->forge->dropTable('cbt_dosen', true);
        $this->forge->dropTable('cbt_matkul', true);
        $this->forge->dropTable('cbt_kuliah', true);
        $this->forge->dropTable('cbt_studi', true);
        $this->forge->dropTable('cbt_ruang', true);
        $this->forge->dropTable('cbt_ujian', true);
        $this->forge->dropTable('cbt_soal', true);
        $this->forge->dropTable('cbt_pilgan', true);

        $db->enableForeignKeyChecks();
    }
}