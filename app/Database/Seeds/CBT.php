<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;
use CodeIgniter\Shield\Authentication\Passwords;
use Config\Auth;

class CBT extends Seeder
{
    public function run()
    {
        $this->call('Auth');
        $this->call('Dosen');
        $this->call('Mahasiswa');
        $this->call('Matkul');
        $this->call('Ruang');
        $this->call('Kuliah');
        $this->call('Studi');
        $this->call('Ujian');
    }
}
