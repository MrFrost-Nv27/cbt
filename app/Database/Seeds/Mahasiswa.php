<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Mahasiswa extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id_user' => 4,
                'nim' => 42419036,
                'nama' => 'M. Reza Aditya',
                'alamat' => "Cilincing",
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_user' => 5,
                'nim' => 42418021,
                'nama' => "Ilham",
                'alamat' => "Tonjong",
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_user' => 10,
                'nim' => 42419010,
                'nama' => "Rudi Juniyanto",
                'alamat' => "Guci",
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_user' => 11,
                'nim' => 42419011,
                'nama' => "Nova Adi Saputra",
                'alamat' => "Suradadi",
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_user' => 12,
                'nim' => 42419012,
                'nama' => "Khusniyati",
                'alamat' => "Benda",
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('cbt_mahasiswa')->emptyTable();
        $this->db->table('cbt_mahasiswa')->insertBatch($data);
    }
}
