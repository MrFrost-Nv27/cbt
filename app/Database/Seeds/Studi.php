<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Studi extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id_kuliah' => 1,
                'id_mahasiswa' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 2,
                'id_mahasiswa' => 2,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 1,
                'id_mahasiswa' => 2,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 1,
                'id_mahasiswa' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 1,
                'id_mahasiswa' => 4,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 1,
                'id_mahasiswa' => 5,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 2,
                'id_mahasiswa' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 2,
                'id_mahasiswa' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 2,
                'id_mahasiswa' => 4,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 2,
                'id_mahasiswa' => 5,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 3,
                'id_mahasiswa' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 3,
                'id_mahasiswa' => 2,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 3,
                'id_mahasiswa' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 3,
                'id_mahasiswa' => 4,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 3,
                'id_mahasiswa' => 5,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 4,
                'id_mahasiswa' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 4,
                'id_mahasiswa' => 2,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 4,
                'id_mahasiswa' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 4,
                'id_mahasiswa' => 4,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 4,
                'id_mahasiswa' => 5,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 5,
                'id_mahasiswa' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 5,
                'id_mahasiswa' => 2,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 5,
                'id_mahasiswa' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 5,
                'id_mahasiswa' => 4,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 5,
                'id_mahasiswa' => 5,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 6,
                'id_mahasiswa' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 6,
                'id_mahasiswa' => 2,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 6,
                'id_mahasiswa' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 6,
                'id_mahasiswa' => 4,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 6,
                'id_mahasiswa' => 5,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('cbt_studi')->emptyTable();
        $this->db->table('cbt_studi')->insertBatch($data);
    }
}
