<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Ruang extends Seeder
{
    public function run()
    {
        $data = [
            [
                'kode' => "R201",
                'nama' => 'Ruang 201',
                'kapasitas' => 40,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "R202",
                'nama' => 'Ruang 202',
                'kapasitas' => 20,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "R301",
                'nama' => "Ruang 301",
                'kapasitas' => 20,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "R302",
                'nama' => "Ruang 302",
                'kapasitas' => 15,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "R303",
                'nama' => "Ruang 303",
                'kapasitas' => 40,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "R303D",
                'nama' => "Ruang 303D",
                'kapasitas' => 40,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('cbt_ruang')->emptyTable();
        $this->db->table('cbt_ruang')->insertBatch($data);
    }
}
