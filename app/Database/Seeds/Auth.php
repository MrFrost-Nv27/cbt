<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;
use CodeIgniter\Shield\Authentication\Passwords;
use Config\Auth as ConfigAuth;

class Auth extends Seeder
{
    public function run()
    {
        $password = new Passwords(new ConfigAuth);
        $data = [
            [
                'id' => 1,
                'username' => 'admin',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 2,
                'username' => 'syauqi',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 3,
                'username' => 'mega',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 4,
                'username' => 'reza',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 5,
                'username' => 'ilham',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 6,
                'username' => 'tezhar',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 7,
                'username' => 'sorikhi',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 8,
                'username' => 'fathulloh',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 9,
                'username' => 'khurotul',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 10,
                'username' => 'rudi',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 11,
                'username' => 'nova',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id' => 12,
                'username' => 'khusni',
                'active' => 1,
                'last_active' => Time::now(),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('users')->emptyTable();
        $this->db->table('users')->insertBatch($data);

        $data = [
            [
                'user_id' => 1,
                'type' => 'email_password',
                'secret' => 'admin@gmail.com',
                'secret2' => $password->hash('admin'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 2,
                'type' => 'email_password',
                'secret' => 'syauqi@gmail.com',
                'secret2' => $password->hash('syauqi'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 3,
                'type' => 'email_password',
                'secret' => 'mega@gmail.com',
                'secret2' => $password->hash('mega'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 4,
                'type' => 'email_password',
                'secret' => 'reza@gmail.com',
                'secret2' => $password->hash('reza'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 5,
                'type' => 'email_password',
                'secret' => 'ilham@gmail.com',
                'secret2' => $password->hash('ilham'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 6,
                'type' => 'email_password',
                'secret' => 'tezhar@gmail.com',
                'secret2' => $password->hash('tezhar'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 7,
                'type' => 'email_password',
                'secret' => 'sorikhi@gmail.com',
                'secret2' => $password->hash('sorikhi'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 8,
                'type' => 'email_password',
                'secret' => 'fathulloh@gmail.com',
                'secret2' => $password->hash('fathulloh'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 9,
                'type' => 'email_password',
                'secret' => 'khurotul@gmail.com',
                'secret2' => $password->hash('khurotul'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 10,
                'type' => 'email_password',
                'secret' => 'rudi@gmail.com',
                'secret2' => $password->hash('rudi'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 11,
                'type' => 'email_password',
                'secret' => 'nova@gmail.com',
                'secret2' => $password->hash('nova'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'user_id' => 9,
                'type' => 'email_password',
                'secret' => 'khusni@gmail.com',
                'secret2' => $password->hash('khusni'),
                'force_reset' => 0,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('auth_identities')->emptyTable();
        $this->db->table('auth_identities')->insertBatch($data);

        $data = [
            [
                'user_id' => 1,
                'group' => 'admin',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 2,
                'group' => 'dosen',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 3,
                'group' => 'dosen',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 4,
                'group' => 'mahasiswa',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 5,
                'group' => 'mahasiswa',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 6,
                'group' => 'dosen',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 7,
                'group' => 'dosen',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 8,
                'group' => 'dosen',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 9,
                'group' => 'dosen',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 10,
                'group' => 'mahasiswa',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 11,
                'group' => 'mahasiswa',
                'created_at' => Time::now(),
            ],
            [
                'user_id' => 12,
                'group' => 'mahasiswa',
                'created_at' => Time::now(),
            ],
        ];
        $this->db->table('auth_groups_users')->emptyTable();
        $this->db->table('auth_groups_users')->insertBatch($data);
    }
}
