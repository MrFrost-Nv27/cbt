<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Ujian extends Seeder
{
    public function run()
    {
        helper('text');
        $data = [
            [
                'id_kuliah' => 1,
                'id_ruang' => 1,
                'waktu' => Time::now(),
                'durasi' => 120,
                'type' => "UAS",
                'token' => strtoupper(random_string('alnum', 8)),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 2,
                'id_ruang' => 2,
                'waktu' => Time::now(),
                'durasi' => 120,
                'type' => "UAS",
                'token' => strtoupper(random_string('alnum', 8)),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 3,
                'id_ruang' => 3,
                'waktu' => Time::now(),
                'durasi' => 120,
                'type' => "UAS",
                'token' => strtoupper(random_string('alnum', 8)),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 4,
                'id_ruang' => 4,
                'waktu' => Time::now(),
                'durasi' => 120,
                'type' => "UAS",
                'token' => strtoupper(random_string('alnum', 8)),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 5,
                'id_ruang' => 5,
                'waktu' => Time::now(),
                'durasi' => 120,
                'type' => "UAS",
                'token' => strtoupper(random_string('alnum', 8)),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_kuliah' => 6,
                'id_ruang' => 6,
                'waktu' => Time::now(),
                'durasi' => 120,
                'type' => "UAS",
                'token' => strtoupper(random_string('alnum', 8)),
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('cbt_ujian')->emptyTable();
        $this->db->table('cbt_ujian')->insertBatch($data);
    }
}
