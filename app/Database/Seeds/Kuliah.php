<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Kuliah extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id_matkul' => 1,
                'id_dosen' => 1,
                'id_ruang' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_matkul' => 2,
                'id_dosen' => 2,
                'id_ruang' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_matkul' => 3,
                'id_dosen' => 5,
                'id_ruang' => 4,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_matkul' => 4,
                'id_dosen' => 6,
                'id_ruang' => 5,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_matkul' => 5,
                'id_dosen' => 4,
                'id_ruang' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'id_matkul' => 6,
                'id_dosen' => 3,
                'id_ruang' => 1,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('cbt_kuliah')->emptyTable();
        $this->db->table('cbt_kuliah')->insertBatch($data);
    }
}
