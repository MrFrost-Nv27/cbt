<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Matkul extends Seeder
{
    public function run()
    {
        $data = [
            [
                'kode' => "Jarnib",
                'nama' => "Jaringan Nirkabel",
                'sks' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "Metopen",
                'nama' => 'Metode Penelitian',
                'sks' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "SIG",
                'nama' => 'Sistem Informasi Geografis',
                'sks' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "APK",
                'nama' => 'Aplikasi Perkantoran',
                'sks' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "DM",
                'nama' => 'Data Mining',
                'sks' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
            [
                'kode' => "IMK",
                'nama' => 'Interaksi Manusia dan Komputer',
                'sks' => 3,
                'created_at' => Time::now(),
                'updated_at' => Time::now(),
            ],
        ];
        $this->db->table('cbt_matkul')->emptyTable();
        $this->db->table('cbt_matkul')->insertBatch($data);
    }
}
