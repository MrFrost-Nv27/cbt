<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class Dosen extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id_user'     => 2,
                'nidn'        => '0604068804',
                'nama'        => 'Achmad Syauqi, M.Kom',
                'alamat'      => "Paguyangan",
                'created_at'  => Time::now(),
                'updated_at'  => Time::now(),
            ],
            [
                'id_user'     => 3,
                'nidn'        => "0606069102",
                'nama'        => "Nurul Mega Saraswati, M.Kom",
                'alamat'      => "Paguyangan",
                'created_at'  => Time::now(),
                'updated_at'  => Time::now(),
            ],
            [
                'id_user'     => 6,
                'nidn'        => "0619019201",
                'nama'        => "Tezhar Rayendra TPN, M.Kom",
                'alamat'      => "Paguyangan",
                'created_at'  => Time::now(),
                'updated_at'  => Time::now(),
            ],
            [
                'id_user'     => 7,
                'nidn'        => "0608087902",
                'nama'        => "Sorikhi, M.Kom",
                'alamat'      => "Pemalang",
                'created_at'  => Time::now(),
                'updated_at'  => Time::now(),
            ],
            [
                'id_user'     => 8,
                'nidn'        => "0623048102",
                'nama'        => "Fathulloh, M.Kom",
                'alamat'      => "Paguyangan",
                'created_at'  => Time::now(),
                'updated_at'  => Time::now(),
            ],
            [
                'id_user'     => 9,
                'nidn'        => "0618098802",
                'nama'        => "Khurotul Aeni, M.Kom",
                'alamat'      => "Paguyangan",
                'created_at'  => Time::now(),
                'updated_at'  => Time::now(),
            ],
        ];
        $this->db->table('cbt_dosen')->emptyTable();
        $this->db->table('cbt_dosen')->insertBatch($data);
    }
}