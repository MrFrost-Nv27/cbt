class FormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        nidn: "",
        nama: "",
        alamat: "",
      },
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    this.setState({
      data: prevProps.data,
    });
  }

  render() {
    return (
      <form method="POST">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="mb-3 row">
                <label for="nidn" class="col-sm-2 col-form-label">
                  NIDN
                </label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="nidn" name="nidn" value={this.state.data.nidn} />
                </div>
              </div>
              <div class="mb-3 row">
                <label for="nama" class="col-sm-2 col-form-label">
                  Nama
                </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="nama" name="nama" />
                </div>
              </div>
              <div class="mb-3 row">
                <label for="alamat" class="col-sm-2 col-form-label">
                  Alamat
                </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="alamat" name="alamat" />
                </div>
              </div>
              <div class="mb-3 row">
                <div class="col text-center">
                  <button type="button" class="btn btn-primary add-button">
                    Simpan
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
