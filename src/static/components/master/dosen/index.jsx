class DosenPage extends React.Component {
  constructor(props) {
    super(props);
    this.pageRef = React.createRef();
    this.state = {
      form: false,
      data: [],
      item: {},
    };
  }

  componentDidMount() {
    this.updateData();
  }

  componentDidUpdate() {
    if (!this.state.form) {
      $(this.pageRef.current).find("table");
    }
  }

  updateData = () => {
    fetch(`${baseUrl}/api/master/dosen`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("gagal");
        }
        return response.json();
      })
      .then((items) => {
        this.setState({
          data: items,
        });
      });
  };

  showForm = (e, data) => {
    let index = e.target.getAttribute("id");
    this.setState({
      form: true,
      item: this.state.data[index],
    });
  };

  closeForm = () => {
    this.setState({
      form: false,
      item: {},
    });
  };

  render() {
    let button, page;
    if (this.state.form) {
      button = (
        <div class="col text-start">
          <button type="button" class="btn btn-primary back-button" onClick={this.closeForm}>
            <i class="mdi mdi-plus"></i>
            Kembali
          </button>
        </div>
      );
      page = <FormInput data={this.state.item} />;
    } else {
      let row = this.state.data.map((item, i) => {
        return (
          <tr>
            <td>{item.nidn}</td>
            <td>{item.nama}</td>
            <td>
              <button class="btn badge rounded-pill text-bg-warning btn-edit" type="button" onClick={this.showForm} id={i}>
                <i class="mdi mdi-pencil"></i>
              </button>
              <button class="btn badge rounded-pill text-bg-danger btn-delete" type="button">
                <i class="mdi mdi-delete"></i>
              </button>
            </td>
          </tr>
        );
      });
      button = (
        <div class="col text-center">
          <button type="button" class="btn btn-primary add-button" onClick={this.showForm}>
            <i class="mdi mdi-plus"></i>
            Tambah
          </button>
        </div>
      );
      page = (
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>NIDN</th>
              <th>Nama</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>{row}</tbody>
        </table>
      );
    }
    return (
      <div class="container">
        <div class="row grid-margin">{button}</div>
        <div class="row">
          <div class="col" ref={this.pageRef}>
            {page}
          </div>
        </div>
      </div>
    );
  }
}

const root = ReactDOM.createRoot(document.querySelector(".state-page"));
root.render(React.createElement(DosenPage));
