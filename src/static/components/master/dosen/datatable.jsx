class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.tableRef = React.createRef();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    this.$table = $(this.tableRef.current);
    this.dt = this.$table.DataTable({
      buttons: [
        {
          extend: "edit",
        },
      ],
      data: this.props.data,
      columns: [
        { data: "nidn", title: "NIDN" },
        { data: "nama", title: "Nama" },
        {
          data: "id",
          title: "Aksi",
          render: () => {
            return ReactDOMServer.renderToString(
              <div>
                <button class="btn badge rounded-pill text-bg-warning btn-edit" type="button">
                  <i class="mdi mdi-pencil"></i>
                </button>
                <button class="btn badge rounded-pill text-bg-danger btn-delete" type="button">
                  <i class="mdi mdi-delete"></i>
                </button>
              </div>
            );
          },
        },
      ],
    });
    this.dt.clear().rows.add(this.props.data).draw();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.data !== this.props.data) {
      this.dt.clear().rows.add(this.props.data).draw();
    }
  }

  render() {
    return (
      <div>
        <table class="table table-striped table-hover" ref={this.tableRef}></table>
      </div>
    );
  }
}
