class DataUjian {
  static async tokenAttempt(data) {
    return $.post(tokenAttemptUrl, data);
  }
  static async waktuUjian() {
    return $.get(ujianUrl + "/waktu");
  }
  static async ujianAttempt(data) {
    return $.post(ujianUrl + "/done", data);
  }
}

export default DataUjian;
