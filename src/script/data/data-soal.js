class DataSoal {
  constructor(index) {
    this.index = index;
    basil.set("soal-index", index);
    this.soal = JSON.parse(basil.get("soal"));
  }

  pindahSoal(index) {
    this.index = index;
    basil.set("soal-index", index);
    return this;
  }

  getJumlahSoal() {
    return this.soal.length;
  }

  getSoalId() {
    return this.soal[this.index].id;
  }

  getSoalText() {
    return this.soal[this.index].text;
  }

  getSoalPilgan() {
    return this.soal[this.index].pilgan;
  }
}

export default DataSoal;
