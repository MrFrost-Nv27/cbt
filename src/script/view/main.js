import dashboard from "./dashboard.js";
import dosen from "./master/dosen.js";
import room from "./ujian/room.js";
import token from "./ujian/token.js";

const main = () => {
  $(".preloader").fadeOut();
  switch (current) {
    case "dashboard":
      dashboard();
      break;
    case "master-data-dosen":
      dosen();
      break;
    case "ujian-token":
      token();
      break;
    case "ujian-room":
      room();
      break;
    default:
      break;
  }
};

export default main;
