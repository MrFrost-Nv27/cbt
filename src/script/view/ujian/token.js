import DataUjian from "../../data/data-ujian.js";

const token = () => {
  console.log(`init ${current} page.....`);
  const page = ReactDOM.createRoot(document.querySelector(".state-page"));
  const tokenForm = $("#token-form");
  const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  tokenForm.on("submit", async function (e) {
    e.preventDefault();
    try {
      const send = await DataUjian.tokenAttempt(tokenForm.serialize());
      Toast.fire({
        icon: "success",
        title: "Token benar",
      }).then((result) => {
        console.log(send);
        if (basil.set("soal", JSON.stringify(send.soal))) {
          location.href = send.url;
        }
      });
    } catch (error) {
      Toast.fire({
        icon: "error",
        title: error.responseJSON.messages.error,
      });
    }
  });
};

export default token;
