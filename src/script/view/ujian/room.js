import DataSoal from "../../data/data-soal.js";
import DataUjian from "../../data/data-ujian.js";
import "../../components/soal-answer.js";

const room = async () => {
  console.log(`init ${current} page.....`);
  const soal = new DataSoal(basil.get("soal-index") ?? 0);
  const page = ReactDOM.createRoot(document.querySelector(".state-page"));
  const formSoal = $(".form-soal");
  const numberWrapper = $(".number-wrapper");
  const numberToggler = $(".number-toggler");
  const answerSelector = "input[name=answer]";
  const answer = $(answerSelector);
  const waktu = (await DataUjian.waktuUjian()).waktu * 1000;

  basil.set("soal-waktu", waktu);
  setTimeout(() => {
    alert("halo");
  }, waktu - Date.now());

  const x = setInterval(function () {
    var distance = waktu - Date.now();

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
      .toString()
      .padStart(2, "0");
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
      .toString()
      .padStart(2, "0");
    var seconds = Math.floor((distance % (1000 * 60)) / 1000)
      .toString()
      .padStart(2, "0");

    $(".waktu").text(hours + ":" + minutes + ":" + seconds);

    if (distance < 0) {
      clearInterval(x);
      $(".waktu").text("EXPIRED");
    }
  }, 1000);

  const init = () => {
    const answer = JSON.parse(basil.get("soal-answer")) ?? [];

    const numberView = $(".number-view");
    let numberViewHtml = "";
    for (let ind = 0; ind < soal.getJumlahSoal(); ind++) {
      numberViewHtml += `
      <div class="number-serve" data-idsoal="${ind}">${ind + 1}</div>
      `;
    }
    numberView.html(numberViewHtml);

    if (soal.index == 0) {
      $(".btn-left").addClass("d-none");
    } else {
      $(".btn-left").removeClass("d-none");
    }

    if (soal.index == soal.getJumlahSoal() - 1) {
      $(".btn-right").html(`<span class="mdi mdi-content-save-edit"></span>`);
    } else {
      $(".btn-right").html(`<span class="mdi mdi-arrow-right"></span>`);
    }

    $(".card-title h4").text(soal.index + 1);
    formSoal.find(".soal-text").text(soal.getSoalText());
    document.querySelector("soal-answer").pilgan = soal.getSoalPilgan();

    if (answer[soal.index] != null) {
      $("input[name=answer][value='" + answer[soal.index] + "']").prop("checked", true);
    }

    activateNumber();
  };

  const saveAnswer = (answer) => {
    let data;
    data = JSON.parse(basil.get("soal-answer")) ?? [];
    data[soal.index] = answer;
    basil.set("soal-answer", JSON.stringify(data));
  };

  const activateNumber = () => {
    const answer = JSON.parse(basil.get("soal-answer")) ?? [];
    $(".number-serve").removeClass("active");
    $.each(answer, function (i, v) {
      if (v != null) {
        $(".number-serve")[i].classList.add("active");
      }
    });
  };

  const done = async (data) => {
    try {
      const send = await DataUjian.ujianAttempt({ answer: data });
      Toast.fire({
        icon: "success",
        title: "Ujian Selesai",
      }).then((result) => {
        console.log(send);
        basil.remove("soal");
        basil.remove("soal-waktu");
        basil.remove("soal-answer");
        basil.remove("soal-index");
        location.href = send.url;
      });
    } catch (error) {
      Toast.fire({
        icon: "error",
        title: error.responseJSON.messages.error,
      });
    }
  };

  init();

  numberToggler.on("click", function () {
    numberWrapper.toggleClass("open");
  });

  $(document).on("click", ".btn-left", function () {
    if (soal.index > 0) {
      soal.pindahSoal(soal.index - 1);
    }
    init();
  });

  $(document).on("click", ".btn-right", function () {
    const answer = JSON.parse(basil.get("soal-answer"));
    console.log(answer);
    if (soal.index < soal.getJumlahSoal() - 1) {
      soal.pindahSoal(soal.index + 1);
    } else {
      Swal.fire({
        title: "Selesai Ujian",
        text: "Apakah anda yakin untuk menyelesaikan ujian ini ?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Batal",
        confirmButtonText: "Selesaikan",
      }).then((result) => {
        if (result.isConfirmed) {
          done(answer);
        }
      });
    }
    init();
  });

  $(document).on("click", ".number-serve", function () {
    soal.pindahSoal($(this).data("idsoal"));
    numberWrapper.removeClass("open");
    init();
  });

  $(document).on("change", answerSelector, function () {
    console.log(formSoal.serialize());
    saveAnswer($(answerSelector + ":checked").val());
    activateNumber();
  });
};

export default room;
