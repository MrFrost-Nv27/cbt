import "./soal-pilgan.js";

class SoalAnswer extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
  }

  set pilgan(pilgan) {
    this._pilgan = pilgan;
    this.updatePilgan();
  }

  updatePilgan() {
    let pilganEl;
    this.innerHTML = "";
    this._pilgan.forEach((pilgan, i) => {
      pilganEl = document.createElement("soal-pilgan");
      pilganEl.index = i;
      pilganEl.pilgan = pilgan;
      this.appendChild(pilganEl);
    });
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name == "value") {
      console.log(newValue);
    }
  }

  static get observedAttributes() {
    return ["value"];
  }

  render() {
    this.innerHTML = ``;
  }
}

customElements.define("soal-answer", SoalAnswer);
