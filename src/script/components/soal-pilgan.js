class SoalPilgan extends HTMLElement {
  constructor() {
    super();
    this.abc = "ABCDE";
  }

  set index(i) {
    this._index = i;
  }

  set pilgan(pilgan) {
    this._pilgan = pilgan;
    this.render();
  }

  render() {
    this.innerHTML = `
    <input class="cbt-abc-box" type="radio" id="${this._pilgan.kode}" name="answer" value="${this._pilgan.kode}">
    <label class="cbt-abc-view" for="${this._pilgan.kode}">
        <div class="d-flex align-items-center">
            <div class="flex-shrink-1"><span class="cbt-abc-index">${this.abc.charAt(this._index)}.</span></div>
            <div class="w-100">
                <p class="m-0 p-0">${this._pilgan.text}</p>
            </div>
        </div>
    </label>
    `;
  }
}

customElements.define("soal-pilgan", SoalPilgan);
